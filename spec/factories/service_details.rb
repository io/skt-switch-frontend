# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :service_detail do
    name "MyString"
    expires_at "2014-06-25"
    description "MyText"
    additional_info "MyText"
    service_id 1
  end
end
