# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :service do
    name "MyString"
    login_address "MyString"
    login_name "MyString"
    login_password "MyString"
    api_key_1 "MyString"
    api_key_2 "MyString"
    service_type 1
  end
end
