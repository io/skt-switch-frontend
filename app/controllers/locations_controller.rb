class LocationsController < ApplicationController
	
  before_action :set_location, only: [:show, :edit, :update, :destroy]
  before_action :load_grid, only: [:index]
  before_action :set_controller_section
  before_filter :authenticate_user!  
  load_and_authorize_resource
  # respond_to :html, :json, :js  
  
  def controller_section
   #self.class.name.split("::").first
   "telephony"   
  end	
           
  def set_controller_section
   @current_section = self.controller_section.titleize
  end	  
        
  # GET /locations
  def index
  end

  # GET /locations/1
  def show
  	  
  end

  # GET /locations/new
  def new
    @location = Location.new
  end

  # GET /locations/1/edit
  #def edit
  #end

  # POST /locations
  def create
    @location = Location.new(location_params)
    respond_to do |format|
      if @location.save
        #format.html { redirect_to @location, notice: 'Location was successfully created. #{make_redo_link}' }
        format.html { redirect_to @location, notice: 'Location was successfully created.' }
        format.js   {}
        format.json { render json: @location, status: :created, location: @location }
      else
        format.html { render action: "new" }
        format.json { render json: @location.errors, status: :unprocessable_entity }
      end
    end
  
  end
                          
  # PATCH/PUT /locations/1
  def update
    @location.update(location_params)     
    flash[:notice] = "Location was successfully updated. #{make_undo_link}" if @location.save
    #respond_with @location          
    render :json => @location
  end                                                 

  # DELETE /locations/1
  def destroy   
    
    respond_to do |format|
      if @location.destroy
        flash[:notice] = 'Location was successfully destroyed.'
        format.js
        format.html
      else
        format.html { render action: "index" }
      end
    end    
    
  end           
  
  def undo
    @location_version = PaperTrail::Version.find_by_id(params[:version_id])
   
    begin
      if @location_version.reify
        @location_version.reify.save
      else
        @location_version.item.destroy
      end
      flash[:notice] = "Update undone. #{make_redo_link}"
    rescue
      flash[:alert] = "Failed undoing the action."
    ensure
      redirect_to locations_path
    end                                                          
  end  
  

  protected
  
    def make_undo_link 
      view_context.link_to 'Undo the last update', undo_location_path(@location.versions.last), method: :post, class: 'alert-link'
    end
                                                
    def make_redo_link
      params[:redo] == "true" ? link = "Undo the last update" : link = "Redo the last update"
      view_context.link_to link, undo_location_path(@location_version.next, redo: !params[:redo]), method: :post, class: 'alert-link'
    end    
    
  private
                       
    def load_grid 
      @locations_grid = initialize_grid(Location, 
      	  per_page: 1000,
      	  name: 'locations',   	  
      	  include: 'legacy_user',
          enable_export_to_csv: true,        
          csv_file_name: 'locations'  	  	  
      )
      export_grid_if_requested
    end        
    
    def set_location
      @location = Location.find(params[:id])
    end

    def location_params
      params.require(:location).permit(:name, :server_ip, :tariff_id)
    end
end
