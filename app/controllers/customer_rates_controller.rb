class CustomerRatesController < ApplicationController
	
  before_action :set_customer_rate, only: [:show, :edit, :update, :destroy]
  before_action :load_grid, only: [:index]
  before_action :load_detail_grid, only: [:show]
  before_action :load_country_rates_grid, only: [:show]
  before_action :set_controller_section
  before_filter :authenticate_user!  
  load_and_authorize_resource
  
  def controller_section
   "services"   
  end	
           
  def set_controller_section
   @current_section = self.controller_section.titleize
  end	  
        
  # GET /customer_rates
  def index
  end

  # GET /customer_rates/1
  def show
  end

  # GET /customer_rates/new
  def new
    @customer_rate = CustomerRate.new
  end

  # GET /customer_rates/1/edit
  #def edit
  #end

  # POST /customer_rates
  def create
    @customer_rate = CustomerRate.new(customer_rate_params)
    respond_to do |format|
      if @customer_rate.save
        #format.html { redirect_to @customer_rate, notice: 'CustomerRate was successfully created. #{make_redo_link}' }
        format.html { redirect_to @customer_rate, notice: 'Customer Rate was successfully created.' }
        format.js   {}
        format.json { render json: @customer_rate, status: :created, location: @customer_rate }
      else
        format.html { render action: "new" }
        format.json { render json: @customer_rate.errors, status: :unprocessable_entity }
      end
    end
  
  end
                          
  # PATCH/PUT /customer_rates/1
  def update
    @customer_rate.update(customer_rate_params)     
    flash[:notice] = "Customer Rate was successfully updated. #{make_undo_link}" if @customer_rate.save
    #respond_with @customer_rate          
    render :json => @customer_rate
  end                                                 

  # DELETE /customer_rates/1
  def destroy   

      if @customer_rate.destroy
        flash[:notice] = 'Customer Rate was successfully destroyed.'
        redirect_to :action => :index
      else
        render :action => :show
      end
      
  end           
  
  def undo
    @customer_rate_version = PaperTrail::Version.find_by_id(params[:version_id])
   
    begin
      if @customer_rate_version.reify
        @customer_rate_version.reify.save
      else
        @customer_rate_version.item.destroy
      end
      flash[:notice] = "Update undone. #{make_redo_link}"
    rescue
      flash[:alert] = "Failed undoing the action."
    ensure
      redirect_to customer_rates_path
    end                                                          
  end  
  
  def empty
    flash[:notice] = 'Customer Rate was successfully emptyed.' if @customer_rate.rates.destroy_all
    # destroy country rates
    redirect_to :back
  end  

  def import  	
      if params[:file]         
        if Rate.import(params[:file], @customer_rate.id)      	      
          flash[:notice] = 'Customer Rate was successfully imported.'
        else
          flash[:error] = 'There was a problem uploading the Customer Rate..'
        end
        redirect_to :back
      end	  
  end

  protected
  
    def make_undo_link 
      view_context.link_to 'Undo the last update', undo_customer_rate_path(@customer_rate.versions.last), method: :post, class: 'alert-link'
    end
                                                
    def make_redo_link
      params[:redo] == "true" ? link = "Undo the last update" : link = "Redo the last update"
      view_context.link_to link, undo_customer_rate_path(@customer_rate_version.next, redo: !params[:redo]), method: :post, class: 'alert-link'
    end    
    
  private
                       
    def load_grid 
      @customer_rates_grid = initialize_grid(CustomerRate.on, 
      	  per_page: 100,
      	  name: 'customer_rates',
          enable_export_to_csv: true,        
          csv_file_name: 'customer_rates'  	  	  
      )
      export_grid_if_requested
    end       
    
    def load_detail_grid 
      @rate_details_grid = initialize_grid(Rate.where(:tariff_id => @customer_rate.id), 
      	  per_page: 100,
      	  name: 'rate_details',
      	  include: ['destination', 'rate_details'],
          enable_export_to_csv: true,        
          csv_file_name: 'rate_details'  	  	  
      )
      export_grid_if_requested
    end      
    
    def load_country_rates_grid 
      @country_rates_grid = initialize_grid(CountryRate.where(:tariff_id => @customer_rate.id), 
      	  per_page: 100,
      	  name: 'country_rates',
          enable_export_to_csv: true,        
          csv_file_name: 'country_rates'  	  	  
      )
      export_grid_if_requested
    end     
    
 
        
    # Use callbacks to share common setup or constraints between actions.
    def set_customer_rate
      @customer_rate = CustomerRate.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def customer_rate_params
      params.require(:customer_rate).permit(:name)
    end
end
