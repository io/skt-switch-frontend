class ErrorsController < ApplicationController 

  layout 'devise'
  
  def show
    render status_code.to_s, :status => status_code
  end
 
  protected
 
  def status_code
    params[:code] || 500
  end
  
  #def status
  #  @exception  = env['action_dispatch.exception']
  #  @status     = ActionDispatch::ExceptionWrapper.new(env, @exception).status_code
  #  @response   = ActionDispatch::ExceptionWrapper.rescue_responses[@exception.class.name]
  #end
  


  
end
