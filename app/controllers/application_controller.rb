class ApplicationController < ActionController::Base

  	  
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?

  before_filter :authenticate_user!	                       
  before_filter :set_locale    
  after_filter :flash_to_headers
  
  add_flash_types :error, :warning

  rescue_from CanCan::AccessDenied do |exception|                         
    redirect_to main_app.root_path, :error => exception.message                    
  end   
  
  def user_for_paper_trail
    user_signed_in? ? current_user.id : 'Guest'
  end  
  	
  protected

  def set_locale
    http_requested_lang = request.env['HTTP_ACCEPT_LANGUAGE'].scan(/^(#{I18n.available_locales.join('|')})/)[0][0] unless request.env['HTTP_ACCEPT_LANGUAGE'].scan(/^(#{I18n.available_locales.join('|')})/).empty? or request.env['HTTP_ACCEPT_LANGUAGE'].nil?     
    I18n.locale = params[:lang] || http_requested_lang || I18n.default_locale
  end
          
  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) do |u| 
      u.permit(:first_name, :email, :role_ids)     
    end    
    devise_parameter_sanitizer.for(:account_update) do |u| 
      u.permit(:first_name, :password, :password_confirmation, :current_password)     
    end
  end  
  
  def flash_to_headers
    return unless request.xhr?
    response.headers['X-Message'] = flash_message
    response.headers["X-Message-Type"] = flash_type.to_s   
    flash.discard
  end  
  
  private

  def flash_class(level)
    case level  
      when :notice then "alert alert-success"
      when :warning then "alert alert-warning"
      when :error then "alert alert-danger"
      when :alert then "alert alert-info"
    end      
  end  
  
  def flash_message
    [:alert, :error, :warning, :notice].each do |type|
      return flash[type] unless flash[type].blank?               
    end   
    return ''
  end
  
  def flash_type
    [:alert, :error, :warning, :notice].each do |type|
      return flash_class(type) unless flash[type].blank?
    end
  end   

end
