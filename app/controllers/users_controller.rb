class UsersController < ApplicationController
	
  before_filter :authenticate_user!
  before_filter :find_user, except: [:index, :new, :create]
  before_action :load_grid, only: [:index]
  before_action :set_controller_section    
  load_and_authorize_resource
  
  def controller_section
   "operator"                      
  end
  
  def index
  end

  def show   
    #@services = @user.services
    #
    #if (@user.has_role? :owner) || (@user.has_role? :admin)
    # @applications = Service.newrelic_api_get("/applications.json")["applications"]
    #         
    # @ovh_servers = Service.ovh_api_get("/dedicated/server")   
    #          
    # @ovh_server_settings = Hash.new; @ovh_server_billing_details = Hash.new; @ovh_servers.each do |s|
    #  @ovh_server_settings[s] = Service.ovh_api_get("/dedicated/server/" + s)
    #  @ovh_server_billing_details[s] = Service.ovh_api_get("/dedicated/server/" + s + "/serviceInfos")
    # end
    #end   	  
  end

  def update 
    @user.update_attributes(secure_params)     
    if @user.save
      flash[:notice] = "User was successfully updated." 
    else
      flash[:error] = "There was a problem updating the user." 
    end
    render :json => @user        
  end

  def destroy
    respond_to do |format|
      @user.destroy    
      flash[:notice] = "User was successfully deleted."
      format.js
      format.html
    end            
  end                          
  
  def reinvite             
    flash[:notice] = "Invitation sent to #{@user.email}" if @user.send_confirmation_instructions
    head :ok   
  end
       
  def reset
    flash[:notice] = "Reset password instructions sent to #{@user.email}" if @user.send_reset_password_instructions
    head :ok 
  end   
  
  def become
    sign_in(:user, User.find(params[:id]))
    redirect_to root_url
  end  
  
  private   
  
  def set_controller_section
   @current_section = self.controller_section.titleize
  end    
  
  def load_grid 
    @users_grid = initialize_grid(User, 
   	  per_page: 25,
   	  include: :company,
   	  name: 'users',
          enable_export_to_csv: true,        
          csv_file_name: 'users'  	  	  
    )
    export_grid_if_requested
  end 
         
  def find_user                                                          
    @user = User.find(params[:id])
    @company = @user.company
    @address = @company.addresses.first
    @billing_detail = @company.billing_detail
  end  

  def secure_params
    params.require(:user).permit(:role, :first_name, :last_name, :email, :password, :password_confirmation, :role_ids)
  end

end
