class CurrenciesController < ApplicationController
	 
  before_filter :authenticate_user! 
  load_and_authorize_resource
  	
  before_action :load_grid, only: [:index]
  before_action :set_controller_section	
	
  
  has_scope :active
	
  def controller_section
   "billing"   
  end	
           
  def set_controller_section
   @current_section = self.controller_section.titleize
  end			   
  	
  def undo
    @currency_version = PaperTrail::Version.find_by_id(params[:version_id])
   
    begin
      if @currency_version.reify
        @currency_version.reify.save
      else
        @currency_version.item.destroy
      end
      flash[:notice] = "Update undone. #{make_redo_link}"
    rescue
      flash[:alert] = "Failed undoing the action."
    ensure
      redirect_to currencies_path
    end                                                          
  end	

  protected
  
    def make_undo_link 
      view_context.link_to 'Undo the last update', undo_currency_path(@currency.versions.last), method: :post, class: 'alert-link'
    end
                                                
    def make_redo_link
      params[:redo] == "true" ? link = "Undo the last update" : link = "Redo the last update"
      view_context.link_to link, undo_currency_path(@currency_version.next, redo: !params[:redo]), method: :post, class: 'alert-link'
    end    
    
  private
        
    def load_grid 
      @currencies_grid = initialize_grid(Currency.active, 
      	  per_page: 1000,
      	  name: 'currencies',  	  
          enable_export_to_csv: true,        
          csv_file_name: 'currencies'  	  	  
      )
      export_grid_if_requested
    end    

    def currency_params
      params.require(:currency).permit()
    end  	
  	   
end