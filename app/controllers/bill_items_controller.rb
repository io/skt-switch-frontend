class BillItemsController < ApplicationController
	 
  before_filter :authenticate_user! 
  load_and_authorize_resource
  	
  before_action :set_bill_item, only: [:show, :edit, :update, :destroy]
  before_action :load_grid, only: [:index]
  before_action :set_controller_section  	
	 	
  respond_to :html, :json
	
  def controller_section
   "billing"   
  end	
           
  def set_controller_section
   @current_section = self.controller_section.titleize
  end	
  	
  def undo
    @bill_item_version = PaperTrail::Version.find_by_id(params[:version_id])
   
    begin
      if @bill_item_version.reify
        @bill_item_version.reify.save
      else
        @bill_item_version.item.destroy
      end
      flash[:notice] = "Update undone. #{make_redo_link}"
    rescue
      flash[:alert] = "Failed undoing the action."
    ensure
      redirect_to bill_items_path
    end                                                          
  end	

  protected
  
    def make_undo_link 
      view_context.link_to 'Undo the last update', undo_bill_item_path(@bill_item.versions.last), method: :post, class: 'alert-link'
    end
                                                
    def make_redo_link
      params[:redo] == "true" ? link = "Undo the last update" : link = "Redo the last update"
      view_context.link_to link, undo_bill_item_path(@bill_item_version.next, redo: !params[:redo]), method: :post, class: 'alert-link'
    end    
    
  private
       
    def load_grid 
      @bill_items_grid = initialize_grid(BillItem, 
      	  per_page: 1000,
      	  name: 'bill_items',  
          include: 'legacy_user',	  
          enable_export_to_csv: true,        
          csv_file_name: 'bill_items'  	  	  
      )
      export_grid_if_requested
    end    
    
    def set_bill_item
      @bill_item = BillItem.find(params[:id])
    end

    def bill_item_params
      params.require(:bill_item).permit()
    end  	
	 
end