class BillingDetailsController < ApplicationController

  before_action :set_billing_detail, only: [:update]
  before_filter :authenticate_user!  
  load_and_authorize_resource
        
  def update                                     
  	  @billing_detail.update_attributes(billing_detail_params)     
  	  flash[:notice] = "Billing Detail was successfully updated." if @billing_detail.save
  	  render :json => @billing_detail 
  end        
  
  private
  
  def set_billing_detail
    @billing_detail = BillingDetail.find(params[:id])
  end              
    
  def billing_detail_params
    params.require(:billing_detail).permit(:last_4_digits)
  end  
  
end
