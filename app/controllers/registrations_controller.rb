class RegistrationsController < Devise::RegistrationsController
	          
  respond_to :html, :json, :js        
  before_filter :update_sanitized_params, if: :devise_controller?
  before_filter :check_permissions , :only => [ :new , :create , :cancel ]
  skip_before_filter :require_no_authentication                                 
  skip_before_filter :authenticate_user!  
  before_action :set_controller_section
  layout 'application'
 
  def check_permissions
    authorize! :create , resource
  end 
  
  def controller_section
   "operator"                      
  end	
  
  def edit
    #if (@user.has_role? :owner) || (@user.has_role? :admin)
    # @applications = Service.newrelic_api_get("/applications.json")["applications"]
    #         
    # @ovh_servers = Service.ovh_api_get("/dedicated/server")   
    #          
    # @ovh_server_settings = Hash.new; @ovh_server_billing_details = Hash.new; @ovh_servers.each do |s|
    #  @ovh_server_settings[s] = Service.ovh_api_get("/dedicated/server/" + s)
    #  @ovh_server_billing_details[s] = Service.ovh_api_get("/dedicated/server/" + s + "/serviceInfos")
    # end 
    #end 
    super
  end
  
  def update
    account_update_params = devise_parameter_sanitizer.sanitize(:account_update)

    # required for settings form to submit when password is left blank
    if account_update_params[:password].blank?
      account_update_params.delete("password")
      account_update_params.delete("password_confirmation")
    end

    @user = User.find(current_user.id)
    if @user.update_attributes(account_update_params)
      set_flash_message :notice, :updated
      # Sign in the user bypassing validation in case their password changed
      sign_in @user, :bypass => true
      redirect_to after_update_path_for(@user)
    else
      render "edit"
    end
  end  
  
  def new
    super
  end
  
  def destroy
    current_user.destroy!
    flash[:notice] = 'Your account was successfully deleted.'
    after_inactive_sign_up_path_for(current_user)
  end
  

  
  def create
    resource = build_resource(secure_params)
    resource.build_info
    role = Role.find(params[:user][:role_ids]) unless params[:user][:role_ids].nil?
    resource.add_role role.name.to_sym    
    if resource.save
      resource.send_confirmation_instructions
      flash[:notice] = "User was successfully created."
      #respond_with resource
      redirect_to :action => :index, :controller => :users
    else
      clean_up_passwords resource
      messages = resource.errors.full_messages.map { |msg| flash.now[:error] = msg }.join
    end
  end                                                                           

  def update_sanitized_params
    devise_parameter_sanitizer.for(:sign_up) {|u| u.permit(:first_name, :last_name, :email, :password, :password_confirmation, :role_ids)}
    devise_parameter_sanitizer.for(:account_update) {|u| u.permit(:first_name, :last_name, :email, :password, :current_password, :password_confirmation)}
  end
  
  private
  
  def set_controller_section
   @current_section = self.controller_section.titleize
  end    

  def secure_params
    params.require(:user).permit(:first_name, :email)
  end  
  
  protected                                                               
         
  def after_inactive_sign_up_path_for(resource)                         
    redirect_to main_app.root_path                                                        
  end                                                                   
       
  def after_sign_up_path_for(resource)                                  
    redirect_to main_app.root_path
  end

end
