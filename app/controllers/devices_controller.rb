class DevicesController < ApplicationController
	
  before_action :set_device, only: [:show, :edit, :update, :destroy]
  before_action :load_grid, only: [:index]
  before_action :set_controller_section
  before_filter :authenticate_user!  
  load_and_authorize_resource
  
  def controller_section
   "telephony"   
  end	
           
  def set_controller_section
   @current_section = self.controller_section.titleize
  end	  
        
  # GET /devices
  def index
  end

  # GET /devices/1
  def show
  	  
  end

  # GET /devices/new
  def new
    @device = Device.new
  end

  # GET /devices/1/edit
  #def edit
  #end

  # POST /devices
  def create
    @device = Device.new(device_params)    
    respond_to do |format|
      if @device.save    	      
      	@voicemail_box = @device.build_voicemail_box :mailbox => @device.id, :fullname => 'VM-' + @device.name, :email => @device.legacy_user.legacy_address.email
      	if @voicemail_box.save
         #format.html { redirect_to @device, notice: 'Device was successfully created. #{make_redo_link}' }
         format.html { redirect_to @device, notice: 'Device was successfully created.' }
         format.js   {}
         format.json { render json: @device, status: :created, location: @device }
        end
      else
        format.html { render action: "new" }
        format.json { render json: @device.errors, status: :unprocessable_entity }
      end
    end
  
  end
                          
  # PATCH/PUT /devices/1
  def update
    @device.update(device_params)     
    flash[:notice] = "Device was successfully updated. #{make_undo_link}" if @device.save
    #respond_with @device          
    render :json => @device
  end                                                 

  # DELETE /devices/1
  def destroy   
    
    respond_to do |format|
      if @device.destroy
        flash[:notice] = 'Device was successfully destroyed.'
        format.js
        format.html
      else
        format.html { render action: "index" }
      end
    end    
    
  end           
  
  def undo
    @device_version = PaperTrail::Version.find_by_id(params[:device_id])
   
    begin
      if @device_version.reify
        @device_version.reify.save
      else
        @device_version.item.destroy
      end
      flash[:notice] = "Update undone. #{make_redo_link}"
    rescue
      flash[:alert] = "Failed undoing the action."
    ensure
      redirect_to devices_path
    end                                                          
  end  
  

  protected
  
    def make_undo_link 
      view_context.link_to 'Undo the last update', undo_device_path(@device.versions.last), method: :post, class: 'alert-link'
    end
                                                
    def make_redo_link
      params[:redo] == "true" ? link = "Undo the last update" : link = "Redo the last update"
      view_context.link_to link, undo_device_path(@device_version.next, redo: !params[:redo]), method: :post, class: 'alert-link'
    end    
    
  private
                       
    def load_grid 
      @devices_grid = initialize_grid(Device, 
      	  per_page: 1000,
      	  name: 'devices',  
          include: 'legacy_user',	  
          enable_export_to_csv: true,        
          csv_file_name: 'devices'  	  	  
      )
      export_grid_if_requested
    end            
 
    def set_device
      @device = Device.find(params[:id])
    end

    def device_params
      params.require(:device).permit(:description, :user_id, :device_type)
    end
end
