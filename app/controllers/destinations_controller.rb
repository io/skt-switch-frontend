class DestinationsController < ApplicationController
  before_action :set_destination, only: [:show, :edit, :update, :destroy]
  before_action :load_grid, only: [:index]
  before_action :set_controller_section
  before_filter :authenticate_user!  
  load_and_authorize_resource
  # respond_to :html, :json, :js  
  
  def controller_section
   #self.class.name.split("::").first
   "services"   
  end	
           
  def set_controller_section
   @current_section = self.controller_section.titleize
  end	  
        
  # GET /destinations
  def index
    # @destinations = Destination.all
    @destination_versions = Version.where(:item_type => 'Destination')
  end

  # GET /destinations/1
  def show
  	  @rates = Rate.where(:tariff_id => @destination.id)
  end

  # GET /destinations/new
  def new
    #@destination = Destination.new
  end

  # GET /destinations/1/edit
  #def edit
  #end

  # POST /destinations
  def create
    @destination = Destination.new(destination_params)
    respond_to do |format|
      if @destination.save
        #format.html { redirect_to @destination, notice: 'Destination was successfully created. #{make_redo_link}' }
        format.html { redirect_to @destination, notice: 'Destination was successfully created.' }
        format.js   {}
        format.json { render json: @destination, status: :created, location: @destination }
      else
        format.html { render action: "new" }
        format.json { render json: @destination.errors, status: :unprocessable_entity }
      end
    end
  
  end
                          
  # PATCH/PUT /destinations/1
  def update
    @destination.update(destination_params)     
    flash[:notice] = "Destination was successfully updated. #{make_undo_link}" if @destination.save
    #respond_with @destination          
    render :json => @destination
  end                                                 

  # DELETE /destinations/1
  def destroy   
    
    respond_to do |format|
      if @destination.destroy
        flash[:notice] = 'Destination was successfully destroyed.'
        format.js
        format.html
      else
        format.html { render action: "index" }
      end
    end    
    
  end           
  
  def undo
    @destination_version = PaperTrail::Version.find_by_id(params[:destination_id])
   
    begin
      if @destination_version.reify
        @destination_version.reify.save
      else
        @destination_version.item.destroy
      end
      flash[:notice] = "Update undone. #{make_redo_link}"
    rescue
      flash[:alert] = "Failed undoing the action."
    ensure
      redirect_to destinations_path
    end                                                          
  end  
  

  protected
  
    def make_undo_link 
      view_context.link_to 'Undo the last update', undo_destination_path(@destination.versions.last), method: :post, class: 'alert-link'
    end
                                                
    def make_redo_link
      params[:redo] == "true" ? link = "Undo the last update" : link = "Redo the last update"
      view_context.link_to link, undo_destination_path(@destination_version.next, redo: !params[:redo]), method: :post, class: 'alert-link'
    end    
    
  private
                       
    def load_grid 
      @destinations_grid = initialize_grid(Destination, 
      	  per_page: 1000,
      	  name: 'destinations',
          enable_export_to_csv: true,        
          csv_file_name: 'destinations'  	  	  
      )
      export_grid_if_requested
    end        
    
 
        
    # Use callbacks to share common setup or constraints between actions.
    def set_destination
      @destination = Destination.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def destination_params
      params.require(:destination).permit(:name, :login_address, :login_name, :login_password, :api_key_1, :api_key_2, :destination_type)
    end
end
