class LegacyUsersController < ApplicationController
  before_action :set_legacy_user, only: [:show, :edit, :update, :destroy]
  before_action :load_grid, only: [:index, :destroy]
  before_action :set_controller_section
  before_filter :authenticate_user!  
  load_and_authorize_resource
  
  def controller_section
   "operator"   
  end	
           
  def set_controller_section
   @current_section = self.controller_section.titleize
  end	  
        
  # GET /legacy_users
  def index
  end

  # GET /legacy_users/1
  def show
  	  @address = @legacy_user.legacy_address
  	  @least_cost_route = @legacy_user.least_cost_route
  	  @payments = @legacy_user.payments
  end

  # GET /legacy_users/new
  def new
    @legacy_user = LegacyUser.new
  end

  # GET /legacy_users/1/edit
  def edit
  end

  # POST /legacy_users
  def create
    @legacy_user = LegacyUser.new(legacy_user_params)                                                                                
    @legacy_address = @legacy_user.build_legacy_address :email => params[:legacy_user][:legacy_address][:email]    
    
    #@legacy_user.owner_id = current_user.id
    respond_to do |format|
      if @legacy_user.save
      	if @legacy_address.save
         @device_group = @legacy_user.build_device_group :address_id => @legacy_address.id
         if @device_group.save
          format.html { redirect_to @legacy_user, notice: "Legacy User was successfully created. #{make_undo_link}" }
          format.js   {}
          format.json { render json: @legacy_user, status: :created, location: @legacy_user }
         end
        end
      else
        format.html { render action: "new" }
        format.json { render json: @legacy_user.errors, status: :unprocessable_entity }
      end
    end
  
  end
                          
  # PATCH/PUT /legacy_users/1
  def update
    @legacy_user.update(legacy_user_params)     
    flash[:notice] = "Legacy User was successfully updated. #{make_undo_link}" if @legacy_user.save
    #respond_with @legacy_user          
    render :json => @legacy_user
  end                                                 

  # DELETE /legacy_users/1
  def destroy  
    respond_to do |format|  	  
      flash[:notice] = "Legacy User was successfully destroyed. #{make_undo_link}" if @legacy_user.destroy
      format.js
      format.html
    end
  end           
  
  def undo
    @legacy_user_version = PaperTrail::Version.find_by_id(params[:version_id])
   
    begin
      if @legacy_user_version.reify
        @legacy_user_version.reify.save
      else
        @legacy_user_version.item.destroy
      end
      flash[:notice] = "Update undone. #{make_redo_link}"
    rescue
      flash[:alert] = "Failed undoing the action."
    ensure
      redirect_to legacy_users_path
    end                                                          
  end  
  

  protected
  
    def make_undo_link 
      view_context.link_to 'Undo the last update', undo_legacy_user_path(@legacy_user.versions.last), method: :post, class: 'alert-link'
    end
                                                
    def make_redo_link
      params[:redo] == "true" ? link = "Undo the last update" : link = "Redo the last update"
      view_context.link_to link, undo_legacy_user_path(@legacy_user_version.next, redo: !params[:redo]), method: :post, class: 'alert-link'
    end    
    
  private
                       
    def load_grid 
      @legacy_users_grid = initialize_grid(LegacyUser.on, 
      	  per_page: 1000,
      	  name: 'legacy_users',
          enable_export_to_csv: true,        
          csv_file_name: 'legacy_users'  	  	  
      )
      export_grid_if_requested
    end 
    
    def set_legacy_user
      @legacy_user = LegacyUser.find(params[:id])
    end
    
    def legacy_user_params
      params.require(:legacy_user).permit(:username, :first_name, :last_name, :usertype, :password)
    end
end
