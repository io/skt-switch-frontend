class LeastCostRoutesController < ApplicationController
  before_action :set_least_cost_route, only: [:show, :edit, :update, :destroy]
  before_action :load_grid, only: [:index]
  before_action :set_controller_section
  before_filter :authenticate_user!  
  load_and_authorize_resource
  # respond_to :html, :json, :js  
  
  def controller_section
   #self.class.name.split("::").first
   "telephony"   
  end	
           
  def set_controller_section
   @current_section = self.controller_section.titleize
  end	  
        
  # GET /least_cost_routes
  def index
    # @least_cost_routes = LeastCostRoute.all
    @least_cost_route_versions = Version.where(:item_type => 'LeastCostRoute')
  end

  # GET /least_cost_routes/1
  def show
  end

  # GET /least_cost_routes/new
  def new
    @least_cost_route = LeastCostRoute.new
  end

  # GET /least_cost_routes/1/edit
  #def edit
  #end

  # POST /least_cost_routes
  def create
    @least_cost_route = LeastCostRoute.new(least_cost_route_params)
    respond_to do |format|
      if @least_cost_route.save
        #format.html { redirect_to @least_cost_route, notice: 'LeastCostRoute was successfully created. #{make_redo_link}' }
        format.html { redirect_to @least_cost_route, notice: 'LeastCostRoute was successfully created.' }
        format.js   {}
        format.json { render json: @least_cost_route, status: :created, location: @least_cost_route }
      else
        format.html { render action: "new" }
        format.json { render json: @least_cost_route.errors, status: :unprocessable_entity }
      end
    end
  
  end
                          
  # PATCH/PUT /least_cost_routes/1
  def update
    @least_cost_route.update(least_cost_route_params)     
    flash[:notice] = "LeastCostRoute was successfully updated. #{make_undo_link}" if @least_cost_route.save
    #respond_with @least_cost_route          
    render :json => @least_cost_route
  end                                                 

  # DELETE /least_cost_routes/1
  def destroy   
    
    respond_to do |format|
      if @least_cost_route.destroy
        flash[:notice] = 'LeastCostRoute was successfully destroyed.'
        format.js
        format.html
      else
        format.html { render action: "index" }
      end
    end    
    
  end           
  
  def undo
    @least_cost_route_version = PaperTrail::Version.find_by_id(params[:least_cost_route_id])
   
    begin
      if @least_cost_route_version.reify
        @least_cost_route_version.reify.save
      else
        @least_cost_route_version.item.destroy
      end
      flash[:notice] = "Update undone. #{make_redo_link}"
    rescue
      flash[:alert] = "Failed undoing the action."
    ensure
      redirect_to least_cost_routes_path
    end                                                          
  end  
  
  def reboot_server
  	  flash[:notice] = "Server was successfully sent a reboot request." if LeastCostRoute.ovh_api_post('/dedicated/server/#{params[:server_name]}/reboot')
  	  
  end

  protected
  
    def make_undo_link 
      view_context.link_to 'Undo the last update', undo_least_cost_route_path(@least_cost_route.versions.last), method: :post, class: 'alert-link'
    end
                                                
    def make_redo_link
      params[:redo] == "true" ? link = "Undo the last update" : link = "Redo the last update"
      view_context.link_to link, undo_least_cost_route_path(@least_cost_route_version.next, redo: !params[:redo]), method: :post, class: 'alert-link'
    end    
    
  private
                       
    def load_grid 
      @least_cost_routes_grid = initialize_grid(LeastCostRoute, 
      	  per_page: 100,
      	  name: 'least_cost_routes',
      	  include: ['suppliers', 'legacy_users'],
          enable_export_to_csv: true,        
          csv_file_name: 'least_cost_routes'  	  	  
      )
      export_grid_if_requested
    end        
    
 
        
    # Use callbacks to share common setup or constraints between actions.
    def set_least_cost_route
      @least_cost_route = LeastCostRoute.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def least_cost_route_params
      params.require(:least_cost_route).permit(:name, :login_address, :login_name, :login_password, :api_key_1, :api_key_2, :least_cost_route_type)
    end
end
