class SettingsController < ApplicationController
  
  before_filter :authenticate_user!	
  before_action :set_controller_section 
  before_action :set_setting_file
  
  def controller_section
   "operator"                      
  end	
  
  def index
  end

  def apply
    head :ok
  end
  

  
  def create
  	  
    if (params[:key] && params[:value])
        	  
      @new_setting = "#{params[:key]}: #{params[:value]}"
      if File.open(@setting_file, 'a+') { |f| f.write(@new_setting) }
        flash[:notice] = "Setting was successfully created."
        redirect_to :action => :index
      else
        flash[:notice] = "There was a problem creating the Setting"
        render :new
      end
    end
  	  
  end
  
  def update
  	  
    params[:settings].each do |key, value|  
      @settings[key] = value
    end
    
    if File.open(@setting_file, 'w') { |f| YAML.dump(@settings, f) }            	  	
      flash[:notice] = "Setting was successfully updated."
      render :json => @settings
    else
      flash[:notice] = "There was a problem updating the Setting"
      render :json => :unprocessable_entity
    end    

  end   
  
  def apply
    #Figaro.application = Figaro::Application.new(environment: "development", path: "/srv/voxbox-service-nodes/config/application.yml")
    #Figaro.load
    #flash[:notice] = "Settings were successfully applied." if Figaro::Rails::Application.new.load
  end

  private
    
  def set_controller_section
   @current_section = self.controller_section.titleize
  end    
  
  def set_setting_file
    @setting_file = "#{Rails.root}/config/application.yml"
    @settings = YAML.load_file(@setting_file)
  end
    
end
