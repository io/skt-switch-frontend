class RatesController < ApplicationController
	
  before_action :set_rate, only: [:show, :edit, :update, :destroy]
  before_action :load_detail_grid, only: [:show]
  before_action :load_grid, only: [:index]
  before_action :set_controller_section
  before_filter :authenticate_user!  
  load_and_authorize_resource  
  
  def controller_section
   "telephony"   
  end	
           
  def set_controller_section
   @current_section = self.controller_section.titleize
  end	  

  # GET /rates/new
  def new
    @rate = Rate.new
  end


  # POST /rates
  def create
    @rate = Rate.new(rate_params)
    respond_to do |format|
      if @rate.save
        #format.html { redirect_to @rate, notice: 'Rate was successfully created. #{make_redo_link}' }
        format.html { redirect_to @rate, notice: 'Rate was successfully created.' }
        format.js   {}
        format.json { render json: @rate, status: :created, location: @rate }
      else
        format.html { render action: "new" }
        format.json { render json: @rate.errors, status: :unprocessable_entity }
      end
    end
  
  end
                          
  # PATCH/PUT /rates/1
  def update
    @rate.update(rate_params)     
    flash[:notice] = "Rate was successfully updated. #{make_undo_link}" if @rate.save
    #respond_with @rate          
    render :json => @rate
  end                                                 

  # DELETE /rates/1
  def destroy   
    
    respond_to do |format|
      if @rate.destroy
        flash[:notice] = 'Rate was successfully destroyed.'
        format.js
        format.html
      else
        format.html { render action: "index" }
      end
    end    
    
  end           
  
  def undo
    @rate_version = PaperTrail::Version.find_by_id(params[:version_id])
   
    begin
      if @rate_version.reify
        @rate_version.reify.save
      else
        @rate_version.item.destroy
      end
      flash[:notice] = "Update undone. #{make_redo_link}"
    rescue
      flash[:alert] = "Failed undoing the action."
    ensure
      redirect_to rates_path
    end                                                          
  end  
  
  def empty
    flash[:notice] = 'Customer rate was successfully emptyed.' if @rate.rates.destroy_all
    redirect_to :back
  end  

  def import  	
      if params[:file]         
        if Rate.import(params[:file], @rate.id)      	      
          flash[:notice] = 'Customer rate was successfully imported.'
        else
          flash[:error] = 'There was a problem uploading the customer rate..'
        end
        redirect_to :back
      end	  
  end

  protected
  
    def make_undo_link 
      view_context.link_to 'Undo the last update', undo_rate_path(@rate.versions.last), method: :post, class: 'alert-link'
    end
                                                
    def make_redo_link
      params[:redo] == "true" ? link = "Undo the last update" : link = "Redo the last update"
      view_context.link_to link, undo_rate_path(@rate_version.next, redo: !params[:redo]), method: :post, class: 'alert-link'
    end    
    
  private
        
    
    def set_rate
      @rate = Rate.find(params[:id])
    end

    def rate_params
      params.require(:rate).permit()
    end
end
