class VersionsController < ApplicationController 
	
  before_filter :authenticate_user!
  before_action :set_controller_section 
  load_and_authorize_resource
	
  def controller_section
   "operator"
  end	
  
  def index     
  	  @user_versions = Version.where(:item_type => 'User')
  	  @company_versions = Version.where(:item_type => 'Company')
  	  @address_versions = Version.where(:item_type => 'Address')
  	  @billing_detail_versions = Version.where(:item_type => 'Billing Detail')
  end
  
  private
  
  def set_controller_section
   @current_section = self.controller_section.titleize
  end    

end
