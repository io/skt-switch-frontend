class CallDetailRecordsController < ApplicationController
  before_action :set_call_detail_record, only: [:show, :edit, :update, :destroy]
  before_action :load_grid, only: [:index]
  before_action :set_controller_section
  before_filter :authenticate_user!  
  load_and_authorize_resource
  # respond_to :html, :json, :js  
  
  def controller_section
   #self.class.name.split("::").first
   "reports"   
  end	
           
  def set_controller_section
   @current_section = self.controller_section.titleize
  end	  
        
  # GET /call_detail_records
  def index
    # @call_detail_records = CallDetailRecord.all
    @call_detail_record_versions = Version.where(:item_type => 'CallDetailRecord')
  end

  # GET /call_detail_records/1
  def show
  	  
  end

  # GET /call_detail_records/new
  def new
    @call_detail_record = CallDetailRecord.new
  end

  # GET /call_detail_records/1/edit
  #def edit
  #end

  # POST /call_detail_records
  def create
    @call_detail_record = CallDetailRecord.new(call_detail_record_params)
    respond_to do |format|
      if @call_detail_record.save
        #format.html { redirect_to @call_detail_record, notice: 'CallDetailRecord was successfully created. #{make_redo_link}' }
        format.html { redirect_to @call_detail_record, notice: 'CallDetailRecord was successfully created.' }
        format.js   {}
        format.json { render json: @call_detail_record, status: :created, location: @call_detail_record }
      else
        format.html { render action: "new" }
        format.json { render json: @call_detail_record.errors, status: :unprocessable_entity }
      end
    end
  
  end
                          
  # PATCH/PUT /call_detail_records/1
  def update
    @call_detail_record.update(call_detail_record_params)     
    flash[:notice] = "CallDetailRecord was successfully updated. #{make_undo_link}" if @call_detail_record.save
    #respond_with @call_detail_record          
    render :json => @call_detail_record
  end                                                 

  # DELETE /call_detail_records/1
  def destroy   
    
    respond_to do |format|
      if @call_detail_record.destroy
        flash[:notice] = 'CallDetailRecord was successfully destroyed.'
        format.js
        format.html
      else
        format.html { render action: "index" }
      end
    end    
    
  end           
  
  def undo
    @call_detail_record_version = PaperTrail::Version.find_by_id(params[:call_detail_record_id])
   
    begin
      if @call_detail_record_version.reify
        @call_detail_record_version.reify.save
      else
        @call_detail_record_version.item.destroy
      end
      flash[:notice] = "Update undone. #{make_redo_link}"
    rescue
      flash[:alert] = "Failed undoing the action."
    ensure
      redirect_to call_detail_records_path
    end                                                          
  end  
  

  protected
  
    def make_undo_link 
      view_context.link_to 'Undo the last update', undo_call_detail_record_path(@call_detail_record.versions.last), method: :post, class: 'alert-link'
    end
                                                
    def make_redo_link
      params[:redo] == "true" ? link = "Undo the last update" : link = "Redo the last update"
      view_context.link_to link, undo_call_detail_record_path(@call_detail_record_version.next, redo: !params[:redo]), method: :post, class: 'alert-link'
    end    
    
  private
                       
    def load_grid 
      @call_detail_records_grid = initialize_grid(CallDetailRecord, 
      	  per_page: 1000,
      	  name: 'call_detail_records',
      	  conditions: ["calldate > ?", Time.now - 2.months],
          order: 'calldate',
          order_direction: 'desc',      	  
          enable_export_to_csv: true,        
          csv_file_name: 'call_detail_records'  	  	  
      )
      export_grid_if_requested
    end        
    
 
        
    # Use callbacks to share common setup or constraints between actions.
    def set_call_detail_record
      @call_detail_record = CallDetailRecord.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def call_detail_record_params
      params.require(:call_detail_record).permit(:name, :server_ip, :tariff_id)
    end
end
