class AddressesController < ApplicationController

  before_action :set_address, only: [:update]
  before_filter :authenticate_user!
  load_and_authorize_resource
        
  def update                                     
  	  @address.update_attributes(address_params)     
  	  flash[:notice] = "Address was successfully updated. #{make_undo_link}" if @address.save
  	  render :json => @address
  end        
  
  def undo
    @address_version = PaperTrail::Version.find_by_id(params[:version_id])
   
    begin
      if @address_version.reify
        @address_version.reify.save
      else
        @address_version.item.destroy
      end
      flash[:notice] = "Update undone. #{make_redo_link}"
    rescue
      flash[:alert] = "Failed undoing the action."
    ensure
      redirect_to users_path  	      
    end                                                          
  end

  private  
  
  def make_undo_link
    view_context.link_to 'Undo the last update', undo_address_path(@address.versions.last), method: :post, class: 'alert-link'
  end
                                              
  def make_redo_link
    params[:redo] == "true" ? link = "Undo the last update" : link = "Redo the last update"
    view_context.link_to link, undo_address_path(@address_version.next, redo: !params[:redo]), method: :post, class: 'alert-link'
  end  
  
  def set_address
    @address = Address.find(params[:id])
  end              
    
  def address_params
    params.require(:address).permit(:country, :street_address, :building_number, :phone_number, :cell_phone, :city, :zip)
  end      
  
end
