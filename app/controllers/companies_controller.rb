class CompaniesController < ApplicationController
	
  before_action :set_company, only: [:update]
  before_filter :authenticate_user!  
  load_and_authorize_resource
  
  
  def update
    @company.update_attributes(company_params)     
    flash[:notice] = "Company was successfully updated. #{make_undo_link}" if @company.save
    render :json => @company
  end        
  
  def undo
    @company_version = PaperTrail::Version.find_by_id(params[:version_id])
   
    begin
      if @company_version.reify
        @company_version.reify.save
      else
        @company_version.item.destroy
      end
      flash[:notice] = "Update undone. #{make_redo_link}"
    rescue
      flash[:alert] = "Failed undoing the action."
    ensure
      redirect_to users_path
    end                                                          
  end

  private  
  
  def make_undo_link
    view_context.link_to 'Undo the last update', undo_company_path(@company.versions.last), method: :post, class: 'alert-link'
  end
                                              
  def make_redo_link
    params[:redo] == "true" ? link = "Undo the last update" : link = "Redo the last update"
    view_context.link_to link, undo_company_path(@company_version.next, redo: !params[:redo]), method: :post, class: 'alert-link'
  end  
  
  def set_company
    @company = Company.find(params[:id])
  end              
    
  def company_params
    params[:company][:business_type] = params[:company][:business_type].to_i
    params.require(:company).permit(:name, :business_type, :suffix, :tax_number, :vat_number, :logo)
  end      
         
end
