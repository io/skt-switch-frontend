class SupplierRatesController < ApplicationController
	
  before_action :set_supplier_rate, only: [:show, :edit, :update, :destroy]
  before_action :load_detail_grid, only: [:show]
  before_action :load_grid, only: [:index]
  before_action :set_controller_section
  before_filter :authenticate_user!  
  load_and_authorize_resource  
  
  def controller_section
   "telephony"   
  end	
           
  def set_controller_section
   @current_section = self.controller_section.titleize
  end	  
        
  # GET /supplier_rates
  def index
  end

  # GET /supplier_rates/1
  def show
  	  @rates = @supplier_rate.rates
  end

  # GET /supplier_rates/new
  def new
    @supplier_rate = SupplierRate.new
  end

  # GET /supplier_rates/1/edit
  #def edit
  #end

  # POST /supplier_rates
  def create
    @supplier_rate = SupplierRate.new(supplier_rate_params)
    respond_to do |format|
      if @supplier_rate.save
        #format.html { redirect_to @supplier_rate, notice: 'SupplierRate was successfully created. #{make_redo_link}' }
        format.html { redirect_to @supplier_rate, notice: 'SupplierRate was successfully created.' }
        format.js   {}
        format.json { render json: @supplier_rate, status: :created, location: @supplier_rate }
      else
        format.html { render action: "new" }
        format.json { render json: @supplier_rate.errors, status: :unprocessable_entity }
      end
    end
  
  end
                          
  # PATCH/PUT /supplier_rates/1
  def update
    @supplier_rate.update(supplier_rate_params)     
    flash[:notice] = "SupplierRate was successfully updated. #{make_undo_link}" if @supplier_rate.save
    #respond_with @supplier_rate          
    render :json => @supplier_rate
  end                                                 

  # DELETE /supplier_rates/1
  def destroy   
    
    respond_to do |format|
      if @supplier_rate.destroy
        flash[:notice] = 'SupplierRate was successfully destroyed.'
        format.js
        format.html
      else
        format.html { render action: "index" }
      end
    end    
    
  end           
  
  def undo
    @supplier_rate_version = PaperTrail::Version.find_by_id(params[:version_id])
   
    begin
      if @supplier_rate_version.reify
        @supplier_rate_version.reify.save
      else
        @supplier_rate_version.item.destroy
      end
      flash[:notice] = "Update undone. #{make_redo_link}"
    rescue
      flash[:alert] = "Failed undoing the action."
    ensure
      redirect_to supplier_rates_path
    end                                                          
  end  
  
  def empty
    flash[:notice] = 'Customer rate was successfully emptyed.' if @supplier_rate.rates.destroy_all
    redirect_to :back
  end  

  def import  	
      if params[:file]         
        if Rate.import(params[:file], @supplier_rate.id)      	      
          flash[:notice] = 'Customer rate was successfully imported.'
        else
          flash[:error] = 'There was a problem uploading the customer rate..'
        end
        redirect_to :back
      end	  
  end

  protected
  
    def make_undo_link 
      view_context.link_to 'Undo the last update', undo_supplier_rate_path(@supplier_rate.versions.last), method: :post, class: 'alert-link'
    end
                                                
    def make_redo_link
      params[:redo] == "true" ? link = "Undo the last update" : link = "Redo the last update"
      view_context.link_to link, undo_supplier_rate_path(@supplier_rate_version.next, redo: !params[:redo]), method: :post, class: 'alert-link'
    end    
    
  private
                       
    def load_grid 
      @supplier_rates_grid = initialize_grid(SupplierRate.on, 
      	  per_page: 100,
      	  name: 'supplier_rates',
          enable_export_to_csv: true,        
          csv_file_name: 'supplier_rates'  	  	  
      )
      export_grid_if_requested
    end       
    
    def load_detail_grid 
      @rate_details_grid = initialize_grid(Rate.where(:tariff_id => @supplier_rate.id), 
      	  per_page: 100,
      	  name: 'rate_details',
      	  include: ['destination', 'rate_details'],
          enable_export_to_csv: true,        
          csv_file_name: 'rate_details'  	  	  
      )
      export_grid_if_requested
    end      
    
    def set_supplier_rate
      @supplier_rate = SupplierRate.find(params[:id])
    end

    def supplier_rate_params
      params.require(:supplier_rate).permit(:name, :login_address, :login_name, :login_password, :api_key_1, :api_key_2, :supplier_rate_type)
    end
end
