class SuppliersController < ApplicationController
	
  before_action :set_supplier, only: [:show, :edit, :update, :destroy]
  before_action :load_grid, only: [:index]
  before_action :set_controller_section
  before_filter :authenticate_user!  
  load_and_authorize_resource
  
  def controller_section
   "telephony"   
  end	
           
  def set_controller_section
   @current_section = self.controller_section.titleize
  end	  
        
  # GET /suppliers
  def index
  end

  # GET /suppliers/1
  def show
  	  
  end

  # GET /suppliers/new
  def new
    @supplier = Supplier.new
  end

  # GET /suppliers/1/edit
  #def edit
  #end

  # POST /suppliers
  def create
    @supplier = Supplier.new(supplier_params)
    respond_to do |format|
      if @supplier.save
        #format.html { redirect_to @supplier, notice: 'Supplier was successfully created. #{make_redo_link}' }
        format.html { redirect_to @supplier, notice: 'Supplier was successfully created.' }
        format.js   {}
        format.json { render json: @supplier, status: :created, location: @supplier }
      else
        format.html { render action: "new" }
        format.json { render json: @supplier.errors, status: :unprocessable_entity }
      end
    end
  
  end
                          
  # PATCH/PUT /suppliers/1
  def update
    @supplier.update(supplier_params)     
    if @supplier.save
      flash[:notice] = "Supplier was successfully updated. #{make_undo_link}"
      render :json => :ok
    else
      render :json => :unprocessable_entity
    end
  end                                                 

  # DELETE /suppliers/1
  def destroy   
    
    respond_to do |format|
      if @supplier.destroy
        flash[:notice] = 'Supplier was successfully destroyed.'
        format.js
        format.html
      else
        format.html { render action: "index" }
      end
    end    
    
  end           
  
  def undo
    @supplier_version = PaperTrail::Version.find_by_id(params[:version_id])
   
    begin
      if @supplier_version.reify
        @supplier_version.reify.save
      else
        @supplier_version.item.destroy
      end
      flash[:notice] = "Update undone. #{make_redo_link}"
    rescue
      flash[:alert] = "Failed undoing the action."
    ensure
      redirect_to suppliers_path
    end                                                          
  end  
  

  protected
  
    def make_undo_link 
      view_context.link_to 'Undo the last update', undo_supplier_path(@supplier.versions.last), method: :post, class: 'alert-link'
    end
                                                
    def make_redo_link
      params[:redo] == "true" ? link = "Undo the last update" : link = "Redo the last update"
      view_context.link_to link, undo_supplier_path(@supplier_version.next, redo: !params[:redo]), method: :post, class: 'alert-link'
    end    
    
  private
                       
    def load_grid 
      @suppliers_grid = initialize_grid(Supplier.on, 
      	  per_page: 1000,
      	  name: 'suppliers',
      	  include: :supplier_rate,
          enable_export_to_csv: true,        
          csv_file_name: 'suppliers'  	  	  
      )
      export_grid_if_requested
    end        
    
 
        
    # Use callbacks to share common setup or constraints between actions.
    def set_supplier
      @supplier = Supplier.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def supplier_params
      params.require(:supplier).permit(:name, :server_ip, :tariff_id)
    end
end
