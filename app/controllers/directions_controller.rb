class DirectionsController < ApplicationController
  before_action :set_direction, only: [:show, :edit, :update, :destroy]
  before_action :load_grid, only: [:index]
  before_action :set_controller_section
  before_filter :authenticate_user!  
  load_and_authorize_resource
  # respond_to :html, :json, :js  
  
  def controller_section
   #self.class.name.split("::").first
   "services"   
  end	
           
  def set_controller_section
   @current_section = self.controller_section.titleize
  end	  
        
  # GET /directions
  def index
  end

  # GET /directions/1
  def show
  	  @rates = Rate.where(:tariff_id => @direction.id)
  end

  # GET /directions/new
  def new
    #@direction = Direction.new
  end

  # GET /directions/1/edit
  #def edit
  #end

  # POST /directions
  def create
    @direction = Direction.new(direction_params)
    respond_to do |format|
      if @direction.save
        #format.html { redirect_to @direction, notice: 'Direction was successfully created. #{make_redo_link}' }
        format.html { redirect_to @direction, notice: 'Direction was successfully created.' }
        format.js   {}
        format.json { render json: @direction, status: :created, location: @direction }
      else
        format.html { render action: "new" }
        format.json { render json: @direction.errors, status: :unprocessable_entity }
      end
    end
  
  end
                          
  # PATCH/PUT /directions/1
  def update
    @direction.update(direction_params)     
    flash[:notice] = "Direction was successfully updated. #{make_undo_link}" if @direction.save
    #respond_with @direction          
    render :json => @direction
  end                                                 

  # DELETE /directions/1
  def destroy   
    
    respond_to do |format|
      if @direction.destroy
        flash[:notice] = 'Direction was successfully destroyed.'
        format.js
        format.html
      else
        format.html { render action: "index" }
      end
    end    
    
  end           
  
  def undo
    @direction_version = PaperTrail::Version.find_by_id(params[:direction_id])
   
    begin
      if @direction_version.reify
        @direction_version.reify.save
      else
        @direction_version.item.destroy
      end
      flash[:notice] = "Update undone. #{make_redo_link}"
    rescue
      flash[:alert] = "Failed undoing the action."
    ensure
      redirect_to directions_path
    end                                                          
  end  
  
  def reboot_server
  	  flash[:notice] = "Server was successfully sent a reboot request." if Direction.ovh_api_post('/dedicated/server/#{params[:server_name]}/reboot')
  	  
  end

  protected
  
    def make_undo_link 
      view_context.link_to 'Undo the last update', undo_direction_path(@direction.versions.last), method: :post, class: 'alert-link'
    end
                                                
    def make_redo_link
      params[:redo] == "true" ? link = "Undo the last update" : link = "Redo the last update"
      view_context.link_to link, undo_direction_path(@direction_version.next, redo: !params[:redo]), method: :post, class: 'alert-link'
    end    
    
  private
                       
    def load_grid 
      @directions_grid = initialize_grid(Direction, 
      	  per_page: 1000,
      	  name: 'directions',
          enable_export_to_csv: true,        
          csv_file_name: 'directions'  	  	  
      )
      export_grid_if_requested
    end        
    
 
        
    # Use callbacks to share common setup or constraints between actions.
    def set_direction
      @direction = Direction.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def direction_params
      params.require(:direction).permit()
    end
end
