class BillsController < ApplicationController
	 
  before_filter :authenticate_user! 
  load_and_authorize_resource
  	
  before_action :set_bill, only: [:show, :edit, :update, :destroy]
  before_action :load_grid, only: [:index]
  before_action :set_controller_section  	
	 	
  layout "pdf", :only => [:show]  
  respond_to :html, :json, :js 
	
  def controller_section
   "billing"   
  end	
           
  def set_controller_section
   @current_section = self.controller_section.titleize
  end			
         
  	def show
  	  @bill = Bill.find(params[:id])   	 
  	  respond_with(@bill) do |format|
  	    format.pdf {}  	                                       
  	  end                                                                                         
  	end    
  	
  def undo
    @bill_version = PaperTrail::Version.find_by_id(params[:version_id])
   
    begin
      if @bill_version.reify
        @bill_version.reify.save
      else
        @bill_version.item.destroy
      end
      flash[:notice] = "Update undone. #{make_redo_link}"
    rescue
      flash[:alert] = "Failed undoing the action."
    ensure
      redirect_to bills_path
    end                                                          
  end	

  protected
  
    def make_undo_link 
      view_context.link_to 'Undo the last update', undo_bill_path(@bill.versions.last), method: :post, class: 'alert-link'
    end
                                                
    def make_redo_link
      params[:redo] == "true" ? link = "Undo the last update" : link = "Redo the last update"
      view_context.link_to link, undo_bill_path(@bill_version.next, redo: !params[:redo]), method: :post, class: 'alert-link'
    end    
    
  private
        
    def load_grid 
      @bills_grid = initialize_grid(Device, 
      	  per_page: 1000,
      	  name: 'bills',  
          include: 'legacy_user',	  
          enable_export_to_csv: true,        
          csv_file_name: 'bills'  	  	  
      )
      export_grid_if_requested
    end    
    
    def set_bill
      @bill = Bill.find(params[:id])
    end

    def bill_params
      params.require(:bill).permit()
    end  	
  	   
end