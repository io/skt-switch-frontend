class BillCodesController < ApplicationController
	 	
  before_filter :authenticate_user! 
  load_and_authorize_resource
 
  before_action :set_bill_code, only: [:show, :edit, :update, :destroy]
  before_action :load_grid, only: [:index]
  before_action :set_controller_section
  
  respond_to :html, :json, :js
  has_scope :active 
        
  def controller_section
   "billing"   
  end	
           
  def set_controller_section
   @current_section = self.controller_section.titleize
  end	
  
   
 	  
  	#def create  
  	#  @bill_code = BillCode.new(params[:bill_code])  	  
  	#  respond_to do |format|
  	#    if @bill_code.save
  	#      flash[:notice] = "Bill Code was successfully created."
  	#      format.js {}
  	#      format.json { render json: @bill_code, status: :created, location: @bill_code }
  	#    else
  	#      #format.html { render action: "new" }
  	#      messages = @bill_code.errors.full_messages.map { |msg| flash.now[:error] = msg }.join 
  	#      format.json { render json: messages.titleize, status: :unprocessable_entity }
  	#    end
  	#  end    
  	#end  
        #
  	#def update    
  	#  @bill_code = BillCode.find(params[:id])  
  	#  @bill_code.update_attributes(params[:bill_code])     
  	#  flash[:notice] = "Bill Code was successfully updated." if @bill_code.save
  	#  respond_with @bill_code
  	#end   
  	#
  	#def destroy             
  	#  @bill_code = BillCode.find(params[:id])
  	#  respond_to do |format|
  	#    if @bill_code.destroy
  	#      flash[:notice] = "Bill Code was successfully deleted."
  	#      format.js {}
  	#      format.json { render json: BillCodesDatatable.new(view_context) }
  	#    else
  	#      format.html { render action: "index" }
  	#      format.json { render json: @bill_code.errors, status: :unprocessable_entity }
  	#    end
  	#  end    
  	#end   

  def undo
    @bill_code_version = PaperTrail::Version.find_by_id(params[:version_id])
   
    begin
      if @bill_code_version.reify
        @bill_code_version.reify.save
      else
        @bill_code_version.item.destroy
      end
      flash[:notice] = "Update undone. #{make_redo_link}"
    rescue
      flash[:alert] = "Failed undoing the action."
    ensure
      redirect_to bill_codes_path
    end                                                          
  end	

  protected
  
    def make_undo_link 
      view_context.link_to 'Undo the last update', undo_bill_code_path(@bill_code.versions.last), method: :post, class: 'alert-link'
    end
                                                
    def make_redo_link
      params[:redo] == "true" ? link = "Undo the last update" : link = "Redo the last update"
      view_context.link_to link, undo_bill_code_path(@bill_code_version.next, redo: !params[:redo]), method: :post, class: 'alert-link'
    end    
    
  private
        
    def load_grid 
      @bill_codes_grid = initialize_grid(BillCode, 
      	  per_page: 1000,
      	  name: 'bill_codes',  	  
          enable_export_to_csv: true,        
          csv_file_name: 'bill_codes'  	  	  
      )
      export_grid_if_requested
    end    
    
    def set_bill_code
      @bill_code = BillCode.find(params[:id])
    end

    def bill_code_params
      params.require(:bill_code).permit()
    end
    
end