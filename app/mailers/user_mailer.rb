class UserMailer < ActionMailer::Base
	
  default :from => ENV["DEVELOPER_EMAIL"]  
  default :bcc => ENV["ADMIN_EMAIL"]      
  
  def test(send_to,send_cc, subject)
    mail(:to => send_to, :cc => send_cc, :subject => subject) do |format|          
      format.html
      format.pdf do
        attachments[user.email.to_s + '.pdf'] = WickedPdf.new.pdf_from_string( render_to_string(:pdf => "user", :template => 'users/show.html.haml', :layout => "pdf.html", :handlers => [:haml]) )
      end                  
    end   
  end 
    
end
