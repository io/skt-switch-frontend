json.array!(@services) do |service|
  json.extract! service, :id, :name, :login_address, :login_name, :login_password, :api_key_1, :api_key_2, :service_type
  json.url service_url(service, format: :json)
end
