json.array!(@service_details) do |service_detail|
  json.extract! service_detail, :id, :name, :expires_at, :description, :additional_info, :service_id
  json.url service_detail_url(service_detail, format: :json)
end
