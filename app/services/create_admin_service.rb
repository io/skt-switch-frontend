class CreateAdminService
  def call
    user = User.find_or_create_by!(email: Rails.application.secrets.admin_email) do |user|
      user.password = Rails.application.secrets.admin_password
      user.password_confirmation = Rails.application.secrets.admin_password
      user.first_name = Rails.application.secrets.admin_name     
      user.add_role :admin
      user.confirm!
      
      address = Address.new do |address|
        address.street_address = Faker::Address.street_address 
        address.country = 'Italy'
        address.building_number = Faker::Address.building_number     
        address.phone_number = Faker::PhoneNumber.phone_number
        address.cell_phone = Faker::PhoneNumber.cell_phone
        address.city = Faker::Address.city
        address.zip = Faker::Address.zip        
        
        company = address.build_company do |company|     
          company.name = 'Voxbox.io'
          company.business_type = 0
          company.vat_number = Faker::Company.duns_number
          company.tax_number = Faker::Company.duns_number
          company.save 
          user.company_id = company.id
          billing_detail = company.build_billing_detail do |billing_detail|
            billing_detail.last_4_digits = '0000'
            billing_detail.save
          end        
        end
        address.save
      end
      
    end
  end
end
