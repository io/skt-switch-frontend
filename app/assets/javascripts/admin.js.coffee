    
#$ -> 
# NProgress.start()    
 
init = ->                  
	
 $("[data-toggle='tooltip']").tooltip container: "body"         
 
 NProgress.done()     
 
 $('input[type="checkbox"]').iCheck
    checkboxClass: 'icheckbox_square-blue'
    increaseArea: '20%'     
    
 #$('table.datagrid').addClass('table table-striped table-bordered')
 #
 #   
 #   
 $('.editable').editable   
   mode: 'popup'
   placement: 'top'
   ajaxOptions:
     type: 'put'
     dataType: 'json'
     beforeSend: (response, newValue) ->        
       NProgress.start()                
     success: (response, newValue) ->
       NProgress.done()    
       
 $('select').select2()
  
 $('.fileinput-button').fileupload() 
  
$(document).on 'click', ".fileinput-button", $('#fileupload').click()          
#$(document).on 'click', ".ladda-button", -> Ladda.create(this).start()    	    
$(document).on 'click', "a.btn:not([data-toggle='modal'])", -> NProgress.start()
$(document).on 'click', ".sub-nav a", -> NProgress.start()
$(document).on 'click', "a[data-original-title='Advanced']", -> NProgress.start()
$(document).on 'click', ".primary-nav a", -> NProgress.start()
$(document).on 'click', "input[type='submit']", -> NProgress.start()
#$(document).on 'click', ".table .col-xs a", -> NProgress.start() 

#$(document).on 'ajax:send', {}, (e, data, status, xhr) -> NProgress.start() 
$(document).on 'ajax:complete', {}, (e, data, status, xhr) -> init()

$(document).on 'ready page:load', -> init()
$(document).on 'ready page:change', -> init()
	

