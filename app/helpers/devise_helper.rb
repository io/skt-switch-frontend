module DeviseHelper

  def resource_name
    :user
  end

  def resource
    @resource ||= User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end
  
  def devise_error_messages?
    resource.errors.empty? ? false : true
  end
  
  
  def devise_flash_messages!
  	  
    return "" if resource.errors.empty?    
    messages = ''; resource.errors.messages.each do |value|
      messages << 'Attention! ' + value[0].to_s.humanize.capitalize + ' ' + value[1][0].to_s + '!' if value[1][0] 
    end
    	        
    html = <<-HTML 
    <div class="alert alert-danger alert-dismissable" id="flash_danger">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>    
      #{messages}
    </div>
    HTML
    
    html.html_safe
  end

end
