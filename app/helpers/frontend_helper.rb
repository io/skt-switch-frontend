module FrontendHelper
	
  def index_header(model = nil, title = nil, icon = nil, options = {})
    content_tag :header, class: 'collections-header' do
    	    content_tag :nav, class: 'pull-right actions' do
    	    	    link_to "new_#{model}".to_sym, :class => 'btn btn-sm btn-success btn-labeled' do    	    	      
    	    	    	    icon = content_tag :span, class: 'btn-label' do
    	    	    	    	    content_tag :i, '', class: icon
    	    	    	    end
    	    	    	    safe_join [icon, title.titleize]
    	    	    end
    	    end
    end.html_safe
  end
  
  def new_header(title = nil)
    content_tag :header, class: 'collections-header' do
    	    head = content_tag :h6, title.titleize, class: 'pull-left' 
    	    nav = content_tag :nav, class: 'pull-right actions' do
    	    	    link_to :back, :class => 'btn btn-sm btn-primary btn-labeled' do    	    	      
    	    	    	    icon = content_tag :span, class: 'btn-label' do
    	    	    	    	    content_tag :i, '', class: 'fui-2-left-open'
    	    	    	    end
    	    	    	    safe_join [icon, 'back'.titleize]
    	    	    end
    	    end
    	    #safe_join [head,nav]
    	    nav
    end
  end  
  
  def show_header(model = nil, buttons = nil, options = {})
    content_tag :header, class: 'collections-header' do
    	    head = content_tag :h5, class: 'pull-left' do
                 link_to send("#{model.class.to_s.underscore}_path",model), class: 'editable', data: { mode: 'popup', container: 'body', placement: 'right', type: 'text', model: "#{model.class.to_s.underscore}", name: 'name', url: send("#{model.class.to_s.underscore}_path",model), 'original-title' => "update #{model.class.to_s}".titleize} do
                      model.name.titleize
                 end
    	    end
    	    nav = content_tag :nav, class: 'pull-right actions' do
    	    	  bb = ''
                  buttons.each do |button| 
    	    	    b = link_to button[:_link], :class => "btn btn-sm btn-#{button[:_class]} btn-labeled", method: button[:_method] do    	    	      
    	    	    	    i = content_tag :span, class: 'btn-label' do
    	    	    	    	    content_tag :i, '', class: button[:_icon]
    	    	    	    end
    	    	    	    safe_join [i, button[:_title].titleize]  
    	    	    end
    	    	    bb = safe_join [bb,b]
    	          end
    	          bb
    	    end
    	    safe_join [head,nav]
    end
  end  

  
  def simple_tab(title=nil)
  	content_tag :nav, class: 'tabs nav nav-tabs' do 
  		content_tag :ul do
  			content_tag :li, class: 'active' do
  				link_to title.titleize, '#one', :'data-toggle'=>'tab'  
  			end
  		end
  	end
  end
  
  def tabs(titles=nil)
  	content_tag :nav, class: 'tabs nav nav-tabs' do 
  		content_tag :ul do
  			tt = ''; _class = 'active'
  			titles.each_with_index do |title,i|  				
  			        t = content_tag :li, class: _class do
  				     link_to title.titleize, "##{(i+1).humanize}", :'data-toggle'=>'tab'  
  			        end
  			        tt = safe_join [tt, t]
  			        _class = ''
  			end
  			tt
  		end
  	end
  end
  
  def simple_tab_content_for(body=nil)
  	content_tag :div, class: 'tab-content' do 
  		content_tag :div, class: 'tab-pane active', id: 'one' do
  			render body
  		end
  	end
  end  
  
  def content_for_tabs(tabs=nil)
  	content_tag :div, class: 'tab-content' do
  		tt = ''; _class = 'active'
  		tabs.each_with_index do |tab,i|  			
  			t = content_tag :div, class: "tab-pane #{_class}", id: "#{(i+1).humanize}" do
  				render tab
  			end 
  			tt = safe_join [tt, t]
  			_class = ''
  		end
  		tt
  	end
  end                                                                                                
  
  def grid_button_for(title=nil,model=nil,button=nil,icon=nil,url=nil, options = {})
  	url = send("#{model.class.to_s.underscore}_path",model) || url
  	link_to url, class: "btn btn-xs btn-#{button} btn-labeled", method: options[:method] do
    	       icon = content_tag :span, class: 'btn-label' do
    	       	    content_tag :i, '', class: icon
    	       end
    	       safe_join [icon, title.titleize]
  	end
  end   
  
  def grid_editable_for(model=nil,val=nil,attribute=nil)
  	link_to send("#{model.class.to_s.underscore}_path",model), class: "editable", data: { mode: 'inline', container: 'body', type: 'text', model: "#{model.class.to_s.underscore}", name: attribute, url: send("#{model.class.to_s.underscore}_path",model), 'original-title' => "update #{model.class.to_s} details".titleize}  do
    	       val.to_s
  	end
  end    
 
        
  def form_field_for(form_field=nil,icon=nil)
  	  content_tag :div, class: 'form-group inputs' do
  	  	  content_tag :div, class: 'col-lg-6' do
  	  	  	  content_tag :div, class: 'input-group' do
  	  	  	  	  icon = content_tag :span, class: 'input-group-addon' do
  	  	  	  	  	  content_tag :i, '', class: icon
  	  	  	  	  end
  	  	  	  	  safe_join [icon, form_field]
  	  	  	  end
  	  	  end
  	  end
  end
  
  def form_submit(form=nil,text=nil,_class=nil,_icon=nil)
  	  text.nil? ? text = 'create' : text
  	  _class.nil? ? _class = 'success' : _class
  	  _icon.nil? ? _icon = 'fui-2-plus-1' : _icon
  	  content_tag :div, class: 'form-group' do
  	  	  content_tag :div, class: 'col-lg-6' do
  	  	  	  "<button type='submit' value='' remote='true' name='commit' class='btn btn-#{_class} btn-labeled pull-right'><span class='btn-label'><i class='#{_icon}'></i></span>#{text.titleize}</button>".html_safe 
  	  	  end
  	  end  	
  end
  
  def cancel_account_link(link=nil,text=nil)
  	  content_tag :div, class: 'form-group' do
  	  	  content_tag :div, class: 'col-lg-6' do
  	  	  	  link_to link, :'data-confirm' => "Are you sure?", :method => :delete, :class => "btn btn-danger btn-labeled pull-right" do
    	    	    	    i = content_tag :span, class: 'btn-label' do
    	    	    	    	    content_tag :i, '', class: 'fui-2-trash'
    	    	    	    end
    	    	    	    safe_join [i, text.titleize]    	  	  	  	  
  	  	  	  end
  	  	  end
  	  end   	
  end
  
  def param_value_table(params=nil)
  	  content_tag :table, class: 'table-heading table-striped table-bordered table' do
  	  	  thead = content_tag :thead do
  	  	  	  content_tag :tr do
  	  	  	  	  h1 = content_tag :th, 'parameter'.titleize
  	  	  	  	  h2 = content_tag :th, 'value'.titleize
  	  	  	  	  safe_join [h1,h2]
  	  	  	  end
  	  	  end
  	  	  tbody = content_tag :tbody do
  	  	  	  rs=''
  	  	  	  params.each do |param|
  	  	  	       r = content_tag :tr do  	  	  	  	  
  	  	  	  	    c1 = content_tag :td, param[0].titleize
  	  	  	  	    c2 = content_tag :td do
  	  	  	  	    	    link_to param[1], '#', class: 'editable', data: { mode: 'inline', name: param[1], container: "body", placement: "left", type: 'text', model: 'node_parameter', url: node_parameter_path, 'original-title' => 'Update Setting'}
  	  	  	  	    end
  	  	  	  	    safe_join [c1,c2]
  	  	  	       end
  	  	  	       rs = safe_join [rs,r]
  	  	  	  end
  	  	  	  rs
  	  	  end 
  	  	  safe_join [thead,tbody]
  	  end
  	  
  end
  
  def model_attr_table(params=nil)
  	  
  	  content_tag :table, class: 'table-heading table-striped table-bordered table' do
  	  	  thead = content_tag :thead do
  	  	  	  h = content_tag :tr do
  	  	  	     h1 = content_tag :th, 'parameter'.titleize
  	  	  	     h2 = content_tag :th, 'value'.titleize
  	  	  	     safe_join [h1,h2]
  	  	  	  end
  	  	  end
  	  	  tbody = content_tag :tbody do
  	  	  	  rr=''
  	  	  	  params.each do |param|
  	  	  	       r = content_tag :tr do 
  	  	  	       	    c1 = content_tag :td, param[:title].titleize
  	  	  	  	    c2 = content_tag :td do
  	  	  	  	    	    if param[:editable].nil?
  	  	  	  	    	      link_to param[:attr].to_s, send("#{param[:model].class.to_s.underscore}_path",param[:model]), class: 'editable', data: { mode: 'inline', name: param[:title], container: "body", placement: "left", type: 'text', model: "#{param[:model].class.to_s.underscore}", url: send("#{param[:model].class.to_s.underscore}_path",param[:model]), 'original-title' => "Update #{param[:model].class.to_s}"}
  	  	  	  	            else
  	  	  	  	              param[:attr].to_s
  	  	  	  	            end  	  	  	  	      	      
  	  	  	  	    end
  	  	  	  	    safe_join [c1,c2]
  	  	  	       end
  	  	  	       rr = safe_join [rr,r]
  	  	  	  end
  	  	  	  rr
  	  	  end 
  	  	  safe_join [thead,tbody]
  	  end
  	  
  end  

            
end
