module ApplicationHelper
	
  def bootstrap_class_for(flash_type)
    case flash_type
      when "notice"   
        "alert-success"     	    
      when "warning"
        "alert-warning" 
      when "error"
        "alert-danger"   
      when "alert"
      	"alert-danger"
      else
        flash_type.to_s
    end
  end
  
  
end
