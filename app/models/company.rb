class Company < ActiveRecord::Base
	
	resourcify
	has_paper_trail
      	
  	has_many 		:users, :autosave => true, :dependent => :destroy   
  	has_many		:addresses, :autosave => true, :dependent => :destroy
  	has_one			:billing_detail, :autosave => true, :dependent => :destroy
  	
  	enum business_type: 	[:self_amployed, :limited_liability_company, :unlimited_company, :holding_company]
	
        accepts_nested_attributes_for :users, :addresses, :billing_detail, :allow_destroy => true 
             
        # Logo Uploader
        mount_uploader :logo, CompanyLogoUploader
  	  	
  	validates_presence_of :name, :message => 'Please specify a Name!'   	
  	validates_presence_of :business_type, :message => 'Please specify a Business Type!'
  	validates_presence_of :suffix, :message => 'Please specify a Corporate Name!'
  	validates_presence_of :tax_number, :message => 'Please specify a Tax Number!'
  	validates_presence_of :vat_number, :message => 'Please specify a VAT Number!'
  
  	default_values  :name => { :value => lambda { Faker::Company.name }, :allows_nil => false }, 
  	                :suffix => { :value => lambda { Faker::Company.suffix }, :allows_nil => false }	
	
end
