class User < ActiveRecord::Base

  rolify
  has_paper_trail
  
  devise 		:database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable, :confirmable, :timeoutable
  
  belongs_to 			:company                     
  
  validates 			:first_name, :email, :presence => true	
  validates 			:email, :format => { :with => /^([A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4})?$/, :message => 'Email must be valid!', :multiline => true }
  validates_uniqueness_of 	:email, :message => 'A user with that email already exists!'
  
  default_values  :email => { :value => lambda { Faker::Internet.email}, :allows_nil => false },
                  :first_name => { :value => lambda { Faker::Name.first_name }, :allows_nil => false },
                  :last_name => { :value => lambda { Faker::Name.last_name }, :allows_nil => true }       
                  
  
  #def set_default_role
  #  self.add_role :user      
  #end               
  
  def name        
    "#{first_name.capitalize} #{last_name.capitalize}"
  end     
         
  # no password is required when the account is created; validate password when the user sets one
  
  #validates_confirmation_of :password                                     
  #def password_required?                                                  
  #  if !persisted?                                                        
  #    #!(password != "")
  #    false
  #  else                                                                  
  #    !password.nil? || !password_confirmation.nil?                       
  #  end                                                                   
  #end    
  
  def password_required?
    super if confirmed?
  end

  def password_match?
    self.errors[:password] << "can't be blank" if password.blank?
    self.errors[:password_confirmation] << "can't be blank" if password_confirmation.blank?
    self.errors[:password_confirmation] << "does not match password" if password != password_confirmation
    password == password_confirmation && !password.blank?
  end  
  
  # Send reset password instructions
  def send_reset_password_instructions                                    
    if self.confirmed?                                                    
      super                                                               
    else                                                                  
      errors.add :base, "You must receive an invitation before you set your password."
    end
  end  
  
  # override Devise method: no password confirmation required
  def confirmation_required?                                              
    false                                                                 
  end                                                                     
  
  # override Devise method: can auth only if confirmed or confirmation period has not expired
  def active_for_authentication?                                          
    confirmed? || confirmation_period_valid?                              
  end                                                                     
  
  # new function to set the password                                      
  def attempt_set_password(params)                                        
    p = {}                                                                
    p[:password] = params[:password]                                      
    p[:password_confirmation] = params[:password_confirmation]            
    update_attributes(p)                                                  
  end                                                                     
  
  # new function to determine whether a password has been set             
  def has_no_password?                                                    
    self.encrypted_password.blank?                                        
  end                                                                     
  
  # new function to provide access to protected method pending_any_confirmation
  def only_if_unconfirmed                                                 
    pending_any_confirmation {yield}                                      
  end     
  
  def build_info 
         address = Address.new do |address|                           
         address.street_address = Faker::Address.street_address 
         address.country = 'Italy'
         address.building_number = Faker::Address.building_number     
         address.phone_number = Faker::PhoneNumber.phone_number
         address.cell_phone = Faker::PhoneNumber.cell_phone
         address.city = Faker::Address.city
         address.zip = Faker::Address.zip        
           
           company = address.build_company do |company|   
             company.name = Faker::Company.name    
             company.business_type = 0
             company.vat_number = Faker::Company.duns_number
             company.tax_number = Faker::Company.duns_number
             company.save 
             self.company_id = company.id
             billing_detail = company.build_billing_detail do |billing_detail|
               billing_detail.last_4_digits = '0000'
               billing_detail.save
             end        
           end
           address.save
         end  		
  	
  end       
                   

end                                    
