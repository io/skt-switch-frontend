class RateDetail < ActiveRecord::Base
	
  establish_connection :mysql2
  self.table_name = 'ratedetails'
  
  has_paper_trail
  
  has_many :supplier_rates, :foreign_key => :rate_id, :through => :rate
  has_many :customer_rates, :foreign_key => :rate_id, :through => :rate
  belongs_to :rate_sheet, :foreign_key => :rate_id, :class_name => 'Rate'
  
  
end
