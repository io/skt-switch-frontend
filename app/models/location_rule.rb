class LocationRule < ActiveRecord::Base

  establish_connection "mysql2"
  self.table_name = 'locationrules'
  
  has_paper_trail
  
  belongs_to :location, :primary_key => :location_id 
  
  default_values  :enabled => { :value => 1 },
                  :minlen => { :value => 1 },
                  :maxlen => { :value => 20 },
                  :lr_type => { :value => "dst" }
  
end




