class Ability
  include CanCan::Ability

  def initialize(user)

    user ||= User.new
    if user.has_role? :admin
      can :manage, :all
    elsif user.has_role? :user
      can :manage, [CallDetailRecord, CustomerRate, Device, LeastCostRoute]
      cannot :manage, [User, Version, Company, Address]
    elsif user.has_role? :reseller
    end

  end
end
