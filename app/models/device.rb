class Device < ActiveRecord::Base
	

  establish_connection "mysql2"
  self.table_name = 'devices'
  bad_attribute_names :type
  
  has_paper_trail
  
  belongs_to :legacy_user, :foreign_key => :user_id
  has_many :device_codecs, :foreign_key => :device_id
  has_one :voicemail_box, :foreign_key => :device_id
  
  after_create :update_legacy_columns, :create_codecs, :update_primary_device
  
  def self.inheritance_column
    nil
  end

  default_values  :secret => { :value => lambda { SecureRandom.hex(8)}},
                  :context => { :value => "mor_local"},
                  :pin => { :value => lambda { Faker::Number.number(6)}},
                  :username => { :value => "temporary"},
                  :ipaddr => { :value => "0.0.0.0"},
                  :subscribemwi => { :value => "no"},
                  :name => { :value => "temporary"},
                  :extension => { :value => lambda { Faker::Number.number(6)}},
                  :accountcode => { :value => 0},
                  :qualify => { :value => 'no'},
                  :nat => { :value => 'yes'}
                  
  def update_legacy_columns
  	self.devicegroup_id = self.legacy_user.device_group.id
        self.name = self.device_type + '-' + self.id.to_s
        self.username = self.name
        self.extension = self.id.to_s       	      
      	self.accountcode = self.id
      	self.save
  end
  
  def create_codecs
  	  DeviceCodec.create :device_id => self.id, :codec_id => 1, :priority => 2
  	  DeviceCodec.create :device_id => self.id, :codec_id => 2, :priority => 3
  	  DeviceCodec.create :device_id => self.id, :codec_id => 5, :priority => 1  	  
  end
  
  def update_primary_device
  	if self.legacy_user.devices.count == 1 then
  		self.legacy_user.primary_device_id = self.id
  	end
  end
  
end




