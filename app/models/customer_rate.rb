class CustomerRate < ActiveRecord::Base	
  
  establish_connection "mysql2"
  self.table_name = 'tariffs'
  
  has_paper_trail
  
  has_many :legacy_users, :foreign_key => :tariff_id
  has_many :rate_details, :foreign_key => :tariff_id, :through => :rates, :autosave => true, :dependent => :destroy
  has_many :rates, :foreign_key => :tariff_id, :autosave => true, :dependent => :destroy
  
  validates_uniqueness_of 	:name, :message => 'A Customer Rate with that name already exists!'
  
  scope :on, -> { where(purpose: 'user_wholesale') }

  default_values  :purpose => { :value => 'user_wholesale' },
                  :currency => { :value => 'EUR' }
  
  
end
