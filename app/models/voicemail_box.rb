class VoicemailBox < ActiveRecord::Base
	

  establish_connection "mysql2"
  self.table_name = 'voicemail_boxes'
    
  belongs_to :device, :foreign_key => :device_id
  
  
  default_values  :context => { :value => "default"},
                  :pager => { :value => ""},
                  :dialout => { :value => ""},
                  :callback => { :value => ""}
  
end




