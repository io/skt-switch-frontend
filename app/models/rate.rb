require 'roo'

class Rate < ActiveRecord::Base
	
  
  establish_connection :mysql2
  self.table_name = 'rates'
  
  has_paper_trail
  
  belongs_to :supplier_rate, :foreign_key => :tariff_id
  belongs_to :customer_rate, :foreign_key => :tariff_id
  belongs_to :destination, :foreign_key => :destination_id
  has_many :rate_details, :foreign_key => :rate_id
  
  def self.import(file,tariff_id)
    begin
    spreadsheet = open_spreadsheet(file)
    (2..spreadsheet.last_row).each do |index|
  	row = spreadsheet.row(index)
  	country_code = row[0]; sub_code = row[1]; rate_per_minute = row[2];
        country_rate = CountryRate.find_or_create_by :tariff_id => tariff_id, :country_code => country_code, :sub_code => sub_code 
  	country_rate.update_attribute(:rate_per_second,rate_per_minute)  	
  	destinations = Destination.where(:direction_code => country_code)
  	destinations.each do |destination|
  	  if destination.destination_group.desttype == sub_code
            rate = Rate.find_or_create_by :tariff_id => tariff_id, :destination_id => destination.id
            rate.update_attribute(:destinationgroup_id, destination.destination_group.id)
  	    rate_detail = RateDetail.find_or_create_by :rate_id => rate.id
  	    rate_detail.update_attribute(:rate,rate_per_minute)
  	  end
  	  
        end
        
    end
    rescue
      false
    end
  end
  
  def self.open_spreadsheet(file)
    case File.extname(file.original_filename)
    when ".csv" then Roo::CSV.new(file.path, csv_options: {col_sep: ";"})
    when ".xls" then Roo::Excel.new(file.path, nil, :ignore)
    when ".xlsx" then Roo::Excelx.new(file.path, nil, :ignore)
    else raise "Unknown file type: #{file.original_filename}"
    end
  end  
  
 

  
end
