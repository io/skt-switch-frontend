class Location < ActiveRecord::Base	

  establish_connection "mysql2"
  self.table_name = 'locations'
  
  has_paper_trail
  
  belongs_to :legacy_user, :foreign_key => :user_id 
  has_many :location_rules, :foreign_key => :location_id
  
  after_create :build_location_rules
  
  default_values  :name => { :value => "Default Location" }                 


  def build_location_rules
    default_rules = Location.find(1).location_rules
    for default_rule in default_rules
      location_rule = LocationRule.new :name => default_rule.name, :enabled => default_rule.cut, :add => default_rule.add, :location_id => self.id; location_rule.save!
    end 
  end
  
end




