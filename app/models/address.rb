class Address < ActiveRecord::Base
      	
	resourcify
	has_paper_trail
        
        belongs_to 	      :company                       	
  	  	
  	validates_presence_of :country, :message => 'Please select a Country!' 
  	validates_presence_of :street_address, :message => 'Please specify a Street Address!'
  	validates_presence_of :city, :message => 'Please specify a City!'
  	validates_presence_of :zip, :message => 'Please specify a ZIP code!'
  
  	default_values  :country => { :value => "Italy", :allows_nil => false }, 
  	                :street_address => { :value => lambda { Faker::Address.street_address }, :allows_nil => false },
  	                :city => { :value => lambda { Faker::Address.city }, :allows_nil => false },
  	                :zip => { :value => lambda { Faker::Address.zip_code }, :allows_nil => false }  	
	
  	                    

  	def human_address
  		self.street_address + ' ' + self.building_number + ', ' +  self.city + ' ZIP/CAP' + self.zip + ', ' +  self.country
  	end
end
