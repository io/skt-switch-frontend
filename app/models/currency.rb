class Currency < ActiveRecord::Base  
	
  establish_connection :mysql2
  self.table_name = 'currencies'
  
  has_paper_trail
  
  has_many :legacy_users, :foreign_key => :user_id 
  
  scope :active, -> { where(:active => 1) }
	
end
