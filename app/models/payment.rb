class Payment < ActiveRecord::Base
	
  
  establish_connection :mysql2
  self.table_name = 'payments'
  bad_attribute_names :hash
  
  has_paper_trail
  
  belongs_to :legacy_user, :primary_key => :user_id 
  belongs_to :bill
  
  scope :paypal, -> { where(paymenttype: 'paypal') }
  scope :paypal_fee, -> { where(paymenttype: 'paypal_fee') }
  scope :manual, -> { where(paymenttype: 'manual') }
  
	
	 
  def self.valid
  	Payment.where{payer_email.not_eq 'NULL'}
  end
   
  def self.to_bill
  	Payment.where{updated_at.eq self.created_at}
  end
  
end
