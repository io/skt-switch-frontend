class SupplierRate < ActiveRecord::Base

  establish_connection "mysql2"
  self.table_name = 'tariffs'
  
  has_paper_trail
  
  has_many :suppliers, :foreign_key => :tariff_id
  has_many :rate_details, :through => :rates, :source => :rate_details, :autosave => true, :dependent => :destroy
  has_many :rates, :foreign_key => :tariff_id, :autosave => true, :dependent => :destroy
  
  validates_uniqueness_of 	:name, :message => 'A Supplier Rate with that name already exists!'
  
  scope :on, -> { where(purpose: 'provider') }
  
  default_values  :purpose => { :value => 'provider' }

  
end
