class LegacyUser < ActiveRecord::Base

  establish_connection :mysql2
  self.table_name = 'users'
  
  has_paper_trail
  
  belongs_to :least_cost_route, :foreign_key => :lcr_id
  belongs_to :legacy_address, :foreign_key => :address_id
  belongs_to :customer_rate, :foreign_key => :tariff_id
  belongs_to :tax, :foreign_key => :tax_id
  
  belongs_to :currency
  has_many :bills  
  
  has_many :payments, :foreign_key => :user_id
  has_many :locations, :foreign_key => :user_id
  has_many :devices, :foreign_key => :user_id, :dependent => :destroy
  has_one :device_group, :foreign_key => :user_id, :autosave => true, :dependent => :destroy                                                                
  
  validates_uniqueness_of :username, :message => 'Username_has_already_been_taken'.titleize
  validates_presence_of :username, :message => 'Username_cannot_be_blank'.titleize  
  
  default_values  :agreement_date => { :value => lambda { Time.now()} },
                  :hide_destination_end => { :value => -1 },
                  :cyberplat_active => { :value => 0 },
                  :generate_invoice => { :value => 0 },
                  :recording_enabled => { :value => 0 },
                  :recording_forced_enabled => { :value => 0 },
                  :recording_hdd_quota => { :value => 0 },
                  :warning_email_active => { :value => 0 },
                  :warning_email_balance => { :value => 0 },
                  :warning_email_hour => { :value => -1 },
                  :invoice_zero_calls => { :value => 1 },  
                  :acc_group_id => { :value => 0 },
                  :credit => { :value => -1 },
                  :own_providers => { :value => 0 },
                  :balance => { :value => 0 },
                  :owner_id => { :value => 0 },
                  :agreement_number => { :value => "0000000" },
                  :time_zone => { :value => 1 },
                  :taxation_country => { :value => 105 },
                  :password => { :value => Digest::SHA1.hexdigest('g3yGbVqb') },
                  :first_name => { :value => "" },
                  :last_name => { :value => "" }
          
  before_destroy :destroy_legacy_related, :destroy_user
  
  after_create :create_user, :update_and_create_legacy_related
  
  scope :on, -> { where(hidden: 0, blocked: 0) }
  
  def self.valid
    Account.where{(first_name.not_eq '') & (vat_number.not_eq '')}
  end
	
  def name        
    "#{first_name} #{last_name}"
  end 
  
  def call_limit_to_human
  	  self.call_limit == 0 ? 'Unlimited' : self.call_limit.to_s
  end
  
  def credit_type_to_human
  	  self.postpaid == 1 ? 'Post-paid' : 'Pre-paid'
  end
    
  def credit_to_human
  	  self.credit == -1 ? 'Unlimited' : self.credit.to_s
  end
  
  def destroy_legacy_related
  	  LeastCostRoute.find(self.least_cost_route.id).destroy if self.least_cost_route
  	  LegacyAddress.find(self.legacy_address.id).destroy if self.legacy_address
  	  Tax.find(self.tax.id).destroy if self.tax
  	  CustomerRate.find(self.customer_rate.id).destroy if self.customer_rate
  end
 
  def create_user
  	  user = User.new :first_name => self.username, :email => self.legacy_address.email
  	  user.build_info     
  	  user.add_role self.usertype.to_sym  
  	  user.send_confirmation_instructions if user.save!
  end
  
  def destroy_user
  	  user = User.find(:email => self.legacy_address.email)
  	  user.destroy if user
  end
  
  def create_digest
    self.password = Digest::SHA1.hexdigest(self.password); self.save!
  end
  
  def update_and_create_legacy_related  
    	  
    if self.usertype.to_s == 'reseller'
       location = self.locations.new; location.save!     
    end                
    
    least_cost_route = self.build_least_cost_route :name => "LCR-" + self.username; least_cost_route.save!; self.lcr_id = least_cost_route.id
    customer_rate = self.build_customer_rate :name => "PR-" + self.username; customer_rate.save!; self.tariff_id = customer_rate.id
    tax = self.build_tax; tax.save!; self.tax_id = tax.id
       
    
    self.save!
    
  end       
  
  #def create_default_device(options={})
  #  owner_id =self.owner_id
  #
  #  fextension = options[:free_ext]
  #  device = Device.new({:user_id => self.id, :devicegroup_id => options[:dev_group].to_i, :context => "mor_local", :device_type => options[:device_type].to_s, :extension => fextension, :pin => options[:pin].to_s, :secret => options[:secret].to_s})
  #  device.description = options[:description] if options[:description]
  #  device.device_ip_authentication_record = options[:device_ip_authentication_record] if options[:device_ip_authentication_record]
  #  device.username = options[:username] ? options[:username] : fextension
  #  device.name = options[:username] ? options[:username] : fextension
  #  device.dtmfmode = Confline.get_value("Default_device_dtmfmode", owner_id).to_s
  #  device.works_not_logged = Confline.get_value("Default_device_works_not_logged", owner_id).to_i
  #  if owner_id != 0
  #    #kvieciam metoda
  #    owner = User.find_by_id(owner_id)
  #    owner.after_create_localization
  #    #after this value should be default location and reseller gets new default location if did not have it
  #  end
  #
  #
  #  # if reseller and location id == 1, create default location and set new location id
  #  set_location_id = Confline.get_value("Default_device_location_id", owner_id)
  #  if (set_location_id.blank? or set_location_id.to_i == 1) and owner and owner.is_reseller?
  #    device.check_location_id
  #    device.location_id = Confline.get_value("Default_device_location_id", owner_id).to_i
  #
  #    logger.fatal('setting location_id:')
  #    logger.fatal(Confline.get_value("Default_device_location_id", owner_id))
  #    logger.fatal('now device location id:')
  #    logger.fatal(device.location_id.to_yaml)
  #
  #  else
  #    device.location_id = set_location_id
  #  end
  #
  #  device.timeout = Confline.get_value("Default_device_timeout", owner_id)
  #
  #  device.record = Confline.get_value("Default_device_record", owner_id).to_i
  #  device.recording_to_email = Confline.get_value("Default_device_recording_to_email", owner_id).to_i
  #  device.recording_keep = Confline.get_value("Default_device_recording_keep", owner_id).to_i
  #  device.record_forced = Confline.get_value("Default_device_record_forced", owner_id).to_i
  #  device.recording_email = Confline.get_value("Default_device_recording_email", owner_id).to_s
  #
  #  device.call_limit = Confline.get_value("Default_device_call_limit", owner_id)
  #  device.server_id = Confline.get_value("Default_device_server_id")
  #
  #
  #  device.nat = Confline.get_value("Default_device_nat", owner_id).to_s
  #
  #  device.voicemail_active = Confline.get_value("Default_device_voicemail_active", owner_id).to_i
  #
  #  device.trustrpid = Confline.get_value("Default_device_trustrpid", owner_id).to_s
  #  device.sendrpid = Confline.get_value("Default_device_sendrpid", owner_id).to_s
  #  device.t38pt_udptl = Confline.get_value("Default_device_t38pt_udptl", owner_id).to_s
  #  device.promiscredir = Confline.get_value("Default_device_promiscredir", owner_id).to_s
  #  device.promiscredir = "no" if device.promiscredir.to_s != "yes" and device.promiscredir.to_s != "no"
  #  device.progressinband = Confline.get_value("Default_device_progressinband", owner_id).to_s
  #  device.videosupport = Confline.get_value("Default_device_videosupport", owner_id).to_s
  #  device.allow_duplicate_calls = Confline.get_value("Default_device_allow_duplicate_calls", owner_id).to_i
  #  device.tell_balance = Confline.get_value("Default_device_tell_balance", owner_id).to_i
  #  device.tell_time = Confline.get_value("Default_device_tell_time", owner_id).to_i
  #  device.tell_rtime_when_left = Confline.get_value("Default_device_tell_rtime_when_left", owner_id).to_i
  #  device.repeat_rtime_every = Confline.get_value("Default_device_repeat_rtime_every", owner_id).to_i
  #  device.qf_tell_time = Confline.get_value("Default_device_qf_tell_time", owner_id).to_i
  #  device.qf_tell_balance = Confline.get_value("Default_device_qf_tell_balance", owner_id).to_i
  #
  #  device.permit = Confline.get_value("Default_device_permits", owner_id).to_s
  #  device.qualify = Confline.get_value("Default_device_qualify", owner_id)
  #
  #  device.host = Confline.get_value("Default_device_host", owner_id).to_s
  #  device.host = "0.0.0.0" if  options[:device_type] == "H323"
  #  device.ipaddr = Confline.get_value("Default_device_ipaddr", owner_id).to_s
  #  device.ipaddr = "0.0.0.0" if  options[:device_type] == "H323"
  #
  #  device.port = Confline.get_value("Default_device_port", owner_id).to_i
  #  device.port = "1720" if  options[:device_type] == "H323"
  #
  #  device.regseconds = Confline.get_value("Default_device_regseconds", owner_id).to_i
  #  device.canreinvite = Confline.get_value("Default_device_canreinvite", owner_id).to_s
  #  device.transfer = Confline.get_value("Default_device_canreinvite", owner_id).to_s
  #  device.istrunk = Confline.get_value("Default_device_istrunk", owner_id).to_i
  #  device.ani = Confline.get_value("Default_device_ani", owner_id).to_i
  #  device.callgroup = Confline.get_value("Default_device_callgroup", owner_id).to_s.blank? ? nil : Confline.get_value("Default_device_callgroup", owner_id).to_i
  #
  #  device.pickupgroup = Confline.get_value("Default_device_pickupgroup", owner_id).to_s.blank? ? nil : Confline.get_value("Default_device_pickupgroup", owner_id).to_i
  #  device.fromuser = Confline.get_value("Default_device_fromuser", owner_id).to_s
  #
  #  device.time_limit_per_day = Confline.get_value("Default_device_time_limit_per_day", owner_id).to_s
  #  device.transport = Confline.get_value("Default_device_transport", owner_id).to_s
  #  device.transport = 'udp' if !['tcp', 'udp', 'tcp,udp', 'udp,tcp'].include?(device.transport)
  #  device.fromdomain = Confline.get_value("Default_device_fromdomain", owner_id).to_s
  #  device.grace_time = Confline.get_value("Default_device_grace_time", owner_id).to_s
  #  device.insecure = Confline.get_value("Default_device_insecure", owner_id).to_s
  #  device.process_sipchaninfo = Confline.get_value("Default_device_process_sipchaninfo", owner_id).to_i
  #  device.fake_ring = Confline.get_value("Default_device_fake_ring", owner_id).to_i
  #  device.enable_mwi = Confline.get_value("Default_device_enable_mwi", owner_id).to_i
  #  device.save_call_log = Confline.get_value("Default_device_save_call_log", owner_id).to_i
  #  device.use_ani_for_cli = Confline.get_value("Default_device_use_ani_for_cli", owner_id)
  #  device.calleridpres = Confline.get_value("Default_device_calleridpres", owner_id).to_s
  #  device.change_failed_code_to = Confline.get_value("Default_device_change_failed_code_to", owner_id).to_i
  #  device.encryption = Confline.get_value("Default_device_encryption", owner_id)
  #  device.block_callerid = Confline.get_value("Default_device_block_callerid", owner_id).to_i
  #  device.max_timeout = Confline.get_value("Default_device_max_timeout", owner_id).to_i
  #  device.language = Confline.get_value("Default_device_language", owner_id).to_s
  #  device.anti_resale_auto_answer = Confline.get_value("Default_device_anti_resale_auto_answer", owner_id).to_i
  #  if not device.works_not_logged
  #    device.works_not_logged = 1
  #  end
  #
  #  if Confline.mor_11_extended?
  #    device.cid_from_dids = Confline.get_value("Default_setting_device_caller_id_number", owner_id).to_i == 3 ? 1 : 0
  #    device.control_callerid_by_cids = Confline.get_value("Default_setting_device_caller_id_number", owner_id).to_i == 4 ? 1 : 0
  #    device.callerid_advanced_control = Confline.get_value("Default_setting_device_caller_id_number", owner_id).to_i == 5 ? 1 : 0
  #  end
  #
  #  if device.save
  #
  #    #      device.accountcode = device.id
  #    #      device.save(false)
  #
  #
  #    #------- VM ----------
  #
  #    pass = Confline.get_value("Default_device_voicemail_box_password", owner_id)
  #    pass = random_digit_password(4) if pass.to_s.length == 0
  #
  #    email = Confline.get_value("Default_device_voicemail_box_email", owner_id)
  #    address = self.address
  #    email = address.email if address and address.email.to_s.size > 0
  #    device.update_cid(Confline.get_value("Default_device_cid_name", owner_id), Confline.get_value("Default_device_cid_number", owner_id), false)
  #    self.primary_device_id = device.id
  #    # configure_extensions(device.id)
  #  end
  #
  #  return device
  #
  #end 
  
end
