class DestinationGroup < ActiveRecord::Base

  establish_connection :mysql2
  self.table_name = 'destinationgroups'
  
  has_paper_trail
  
  has_many :destinations, :foreign_key => :destinationgroup_id
  has_many :rates, :foreign_key => :destinationgroup_id, :through => :destinations

  
end
