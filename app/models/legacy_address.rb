class LegacyAddress < ActiveRecord::Base
	
  
  establish_connection :mysql2
  self.table_name = 'addresses'
  
  has_paper_trail
  
  has_one :legacy_user, :foreign_key => :address_id
  has_one :devicegroup, :foreign_key => :address_id
  belongs_to :direction, :foreign_key => :direction_id
  
  default_values  :direction_id => { :value => 105 },
                  :state => { :value => "Italy" },
                  :county => { :value => "Lombardia" },
                  :city => { :value => "Milano" },
                  :address => { :value => "" },
                  :phone => { :value => "" },
                  :mob_phone => { :value => "" },
                  :fax => { :value => "" },
                  :email => { :value => "" }
  
end
