class Destination < ActiveRecord::Base

  establish_connection :mysql2
  self.table_name = 'destinations'
  
  has_paper_trail
  
  has_many :rates, :foreign_key => :destination_id
  belongs_to :destination_group, :foreign_key => :destinationgroup_id
  
end
