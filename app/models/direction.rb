class Direction < ActiveRecord::Base
	
  
  establish_connection :mysql2
  self.table_name = 'directions'
  
  has_paper_trail
  
  has_many :legacy_addresses, :foreign_key => :direction_id
  
  

  
end
