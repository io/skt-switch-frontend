class Supplier < ActiveRecord::Base
	
  establish_connection "mysql2"
  self.table_name = 'providers'
  
  has_paper_trail
  
  belongs_to :supplier_rate, :foreign_key => :tariff_id
  belongs_to :device, :foreign_key => :device_id
  has_many :least_cost_route_details, :foreign_key => :provider_id
  has_many :least_cost_routes, :through => :least_cost_route_details, :foreign_key => :provider_id
  
  scope :on, -> { where(hidden: 0) }

  default_values  :login => { :value => lambda { Faker::Name.first_name}, :allows_nil => false },
                  :password => { :value => '', :allows_nil => false },
                  :server_ip => { :value => '127.0.0.1', :allows_nil => false },
                  :tech => { :value => 'SIP', :allows_nil => false }
  
  def call_limit_to_human
  	  self.call_limit == 0 ? 'Unlimited' : self.call_limit.to_s
  end
    
end






