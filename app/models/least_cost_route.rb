class LeastCostRoute < ActiveRecord::Base
	
  
  establish_connection :mysql2
  self.table_name = 'lcrs'
  
  has_paper_trail
  
  has_many :legacy_users, :foreign_key => :lcr_id
  has_many :least_cost_route_details, :foreign_key => :lcr_id, :autosave => true, :dependent => :destroy
  has_many :suppliers, :through => :least_cost_route_details, :foreign_key => :provider_id
                  
  def failover_to_human
  	  self.no_failover == 0 ? 'Yes' : 'No'
  end  
  
  
end
