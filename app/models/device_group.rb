class DeviceGroup < ActiveRecord::Base
	
  
  establish_connection "mysql2"
  self.table_name = 'devicegroups'
  
  belongs_to :legacy_user, :primary_key => :user_id
  belongs_to :legacy_address, :primary_key => :address_id
  
  default_values  :name => { :value => 'primary' },
                  :primary => { :value => 1 } 
  
end




