class LeastCostRouteDetail < ActiveRecord::Base
	
  
  establish_connection "mysql2"
  self.table_name = 'lcrproviders'
  
  has_paper_trail
  
  belongs_to :least_cost_route, :foreign_key => :lcr_id
  belongs_to :supplier, :foreign_key => :provider_id
  
end
