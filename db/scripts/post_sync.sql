ALTER TABLE payments   
  CHANGE `user_id` `account_id` VARCHAR(255) CHARSET utf8 COLLATE utf8_unicode_ci NULL,
  CHANGE `hash` `hash_legacy` VARCHAR(255) CHARSET utf8 COLLATE utf8_unicode_ci NULL,
  CHANGE `created_at` `created_at` DATETIME NOT NULL,
  CHANGE `updated_at` `updated_at` DATETIME NOT NULL;
  
UPDATE payments SET created_at = (SELECT NOW()) where created_at = "0000-00-00 00:00:00";  
UPDATE payments SET updated_at = (SELECT NOW());
  
RENAME TABLE users TO accounts;  

RENAME TABLE users_ TO users;

ALTER TABLE accounts   
  CHANGE `created_at` `created_at` DATETIME NOT NULL,
  CHANGE `updated_at` `updated_at` DATETIME NOT NULL;
  
UPDATE accounts SET created_at = (SELECT NOW()) where created_at = "0000-00-00 00:00:00"; 
UPDATE accounts SET updated_at = (SELECT NOW());
  
ALTER TABLE addresses   
  CHANGE `created_at` `created_at` DATETIME NOT NULL,
  CHANGE `updated_at` `updated_at` DATETIME NOT NULL;  

UPDATE addresses SET created_at = (SELECT NOW()) where created_at = "0000-00-00 00:00:00";  
UPDATE addresses SET updated_at = (SELECT NOW()); 
