class CreateBillCodes < ActiveRecord::Migration
  def change
    create_table :bill_codes do |t|
      t.string :bill_code
      t.integer :bill_code_start_from
      t.integer :bill_code_status

      t.timestamps
    end
  end
end
