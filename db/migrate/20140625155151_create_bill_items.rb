class CreateBillItems < ActiveRecord::Migration
  def change
    create_table :bill_items do |t|
      t.float :bill_item_vat
      t.string :bill_item_description
      t.float :bill_item_unit_cost
      t.float :bill_item_unit_fee
      t.float :bill_item_unit_gross
      t.integer :bill_item_quantity
      t.integer :bill_item_discount
      t.integer :bill_id
      t.datetime :bill_item_processed_at
      t.string :bill_item_transaction_id
      t.string :bill_item_currency
      t.integer :client_id
      t.integer :company_id

      t.timestamps
    end
  end
end
