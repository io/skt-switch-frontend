class CreateBillingDetails < ActiveRecord::Migration
  def change
    create_table :billing_details do |t|
    	    
      t.integer :company_id
      t.string :last_4_digits


      t.timestamps
    end
  end
end
