class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :name
      t.integer :business_type		
      t.string :suffix		
      t.string :tax_number
      t.string :vat_number
      t.string :logo

      t.timestamps
    end
  end
end
