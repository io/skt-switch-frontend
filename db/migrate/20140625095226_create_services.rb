class CreateServices < ActiveRecord::Migration
  def change
    create_table :services do |t|
      t.string :name
      t.string :login_address
      t.string :login_name
      t.string :login_password
      t.string :api_key_1
      t.string :api_key_2
      t.integer :service_type

      t.timestamps
    end
  end
end
