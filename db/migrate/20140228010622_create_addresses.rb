class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.string :country
      t.string :street_address
      t.string :building_number
      t.string :phone_number
      t.string :cell_phone
      t.string :city
      t.string :zip
      t.integer :company_id

      t.timestamps
    end
  end
end
