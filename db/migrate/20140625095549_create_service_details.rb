class CreateServiceDetails < ActiveRecord::Migration
  def change
    create_table :service_details do |t|
      t.string :name
      t.date :expires_at
      t.text :description
      t.text :additional_info
      t.integer :service_id

      t.timestamps
    end
  end
end
