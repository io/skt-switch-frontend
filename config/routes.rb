Rails.application.routes.draw do

  resources :addresses
  resources :companies
  resources :billing_details
  resources :settings  
  resources :versions
  resources :service_details
  resources :least_cost_routes
  resources :suppliers
  #resources :supplier_rates, shallow: true do
  #	  resources :rates, :as => :supplier_rate_details
  #end                  
  resources :supplier_rates
  resources :rates                  
  #resources :customer_rates, shallow: true do
  #	  resources :rates, :as => :customer_rate_details
  #end  
  resources :customer_rates
  resources :rates            
  resources :rate_details
  resources :legacy_users
  resources :legacy_addresses
  resources :destinations
  resources :directions
  resources :call_detail_records
  resources :devices
  resources :locations
  resources :location_rules
  resources :country_rates
  
  resources :bill_codes
  resources :bill_items, :only => [:index]
  resources :bills, :only => [:index, :show]
  resources :currencies, :only => [:index]  
  
  resources :rate_details
  
  devise_for :users, :controllers => { :passwords => "passwords", :registrations => "registrations", :confirmations => "confirmations" }

  resources :users  
  resources :users, :only => [:show, :index] do  
  	  member do
  	  	get 'invite'  
  	  	put 'reinvite'
  	  	put 'reset'
  	  end

  end
  
  unauthenticated do      
    devise_scope :user do
      root :to => "devise/sessions#new"
      patch '/user/confirmation' => 'confirmations#update', :via => :patch, :as => :update_user_confirmation
    end
  end
  
  authenticated :user do
    devise_scope :user do
      root :to => "users#index", :as => :authenticated_root
      
      match '/supplier_rates/import' => 'supplier_rates#import', :via => [:put, :post], :as => :import_supplier_rates
      match '/customer_rates/import' => 'customer_rates#import', :via => [:put, :post], :as => :import_customer_rates

      post '/supplier_rates/:id/empty' => 'supplier_rates#empty', :as => :empty_supplier_rate
      post '/customer_rates/:id/empty' => 'customer_rates#empty', :as => :empty_customer_rate      
      
      match '/settings/update' => 'settings#update', :via => :put
      post '/settings/apply' => 'settings#apply', :as => :apply_settings
      post '/users/:id/become' => 'users#become', :as => :become_user
      post '/companies/:version_id/undo', to: 'companies#undo', as: :undo_company
      post '/addresses/:version_id/undo', to: 'addresses#undo', as: :undo_address
      post '/least_cost_routes/:version_id/undo', to: 'least_cost_routes#undo', as: :undo_least_cost_route
      post '/suppliers/:version_id/undo', to: 'suppliers#undo', as: :undo_supplier
      post '/locations/:version_id/undo', to: 'locations#undo', as: :undo_location    
      post '/supplier_rates/:version_id/undo', to: 'supplier_rates#undo', as: :undo_supplier_rate
      post '/legacy_users/:version_id/undo', to: 'legacy_users#undo', as: :undo_legacy_user
    end
  end 
        
  resources :errors, :only => [:show]
  
  
  %w( 404 422 500 ).each do |error_code|
    get error_code, :to => "errors#show", :code => error_code
  end
  match '*a', :to => 'errors#404', :via => :get   

  #match '/404', :to => 'errors#not_found', :via => :get
  #match '/422', :to => 'errors#server_error', :via => :get
  #match '/500', :to => 'errors#server_error', :via => :get 
  
  
end
