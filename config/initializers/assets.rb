Rails.application.config.assets.precompile = ['*.js', '*.css']
Rails.application.config.assets.precompile += %w( flags/*.css )
Rails.application.config.assets.precompile += %w( flags/*.png )
Rails.application.config.assets.precompile += %w( editable/* )
